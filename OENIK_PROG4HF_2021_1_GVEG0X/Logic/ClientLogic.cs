﻿//-----------------------------------------------------------------------
// <copyright file="ClientLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Logic
{
    using System;
    using System.Linq;
    using Models;
    using Repos;

    /// <summary>
    /// A class that handles the logic behind the client repository.
    /// </summary>
    public class ClientLogic
    {
        private readonly IRepoBase<GymClient> clientRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientLogic"/> class.
        /// </summary>
        /// <param name="clientRepo">The existing repository.</param>
        public ClientLogic(IRepoBase<GymClient> clientRepo)
        {
            this.clientRepo = clientRepo;
        }

        /// <summary>
        /// Calls the method in the client repository to add a new client.
        /// </summary>
        /// <param name="gymClient">The client who will be added to the repo.</param>
        public void AddClient(GymClient gymClient)
        {
            this.clientRepo.Add(gymClient);
        }

        /// <summary>
        /// Calls the method in the client repository to delete a client.
        /// </summary>
        /// <param name="clientId">The client's id who needs to be deleted.</param>
        public void DeleteClient(string clientId)
        {
            this.clientRepo.Delete(clientId);
        }

        /// <summary>
        /// Calls the method in the client repository to get all clients.
        /// </summary>
        /// <returns>All the clients in the client repo.</returns>
        public IQueryable<GymClient> GetClients()
        {
            return this.clientRepo.Read();
        }

        /// <summary>
        /// Calls the method in the client repository to return a specific client.
        /// </summary>
        /// <param name="clientId">The client's id who needs to be found.</param>
        /// <returns>The desired client.</returns>
        public GymClient GetClient(string clientId)
        {
            return this.clientRepo.GetItem(clientId);
        }

        /// <summary>
        /// Calls the method in the client repository to update a specific client's properties.
        /// </summary>
        /// <param name="clientId">The client's id who needs to be updated.</param>
        /// <param name="newClient">A Gymclient object with the new desired properties.</param>
        public void UpdateClient(string clientId, GymClient newClient)
        {
            this.clientRepo.Update(clientId, newClient);
        }

        /// <summary>
        /// A method that returns the longest "information" string found in any client.
        /// </summary>
        /// <returns>A string that is the longest information in any client.</returns>
        public string LongestInfo()
        {
            int counter = 0;
            string helper = string.Empty;

            var something = this.GetClients().ToList();
            foreach (var item in something)
            {
                foreach (var info in item.ExtraInfos)
                {
                    if (info.Information.Length > counter)
                    {
                        counter = info.Information.Length;
                        helper = info.Information;
                    }
                }
            }

            return helper;
        }

        /// <summary>
        /// A method that returns the amount of alcoholics from the gym.
        /// </summary>
        /// <returns>Amount of alcoholics.</returns>
        public int AmountOfAlcoholists()
        {
            int counter = 0;
            var something = this.GetClients().ToList();
            foreach (var item in something)
            {
                foreach (var infos in item.ExtraInfos)
                {
                    if (infos.Information.ToLower(new System.Globalization.CultureInfo("en-US")) is "alkoholista" or
                        "alkoholist" or
                        "alcoholist" or
                        "alcoholista" or
                        "alcoholic")
                    {
                        counter++;
                    }
                }
            }

            return counter;
        }

        /// <summary>
        /// A method that adds an ExtraInfo to a client.
        /// </summary>
        /// <param name="extraInfo">ExtraInfo type object that needs be added to a client.</param>
        /// <param name="clientId">The client's id.</param>
        public void AddInfoToClient(ExtraInfo extraInfo, string clientId)
        {
            if (extraInfo != null)
            {
                extraInfo.GymID = clientId;
                this.GetClient(clientId).ExtraInfos.Add(extraInfo);

                this.clientRepo.Save();
            }
        }

        /// <summary>
        /// A method that removes an ExtraInfo from a client.
        /// </summary>
        /// <param name="extraInfo">ExtraInfo type object that needs be deleted from a client.</param>
        /// <param name="clientId">The client's id.</param>
        public void RemoveInfoFromClient(ExtraInfo extraInfo, string clientId)
        {
            this.GetClient(clientId).ExtraInfos.Remove(extraInfo);
            this.clientRepo.Save();
        }

        /// <summary>
        /// A function that fills the database with samples.
        /// </summary>
        public void FillDbWithSamples()
        {
            ExtraInfo i0 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Hardcore wowos" };
            ExtraInfo i1 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Nem szeret lábazni" };
            ExtraInfo i2 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Váll problémái vannak" };
            ExtraInfo i3 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Nem szeret élni" };
            ExtraInfo i4 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Depressziós" };
            ExtraInfo i5 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Heti 9szer eddz" };
            ExtraInfo i6 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Heti 3szer eddz" };
            ExtraInfo i7 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Heti 2szer eddz" };
            ExtraInfo i8 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Heti 8szer eddz" };
            ExtraInfo i9 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Heti 6szer eddz" };
            ExtraInfo i10 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Heti 16szer eddz" };
            ExtraInfo i11 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Egyáltalán ezt valaki elolvassa?" };
            ExtraInfo i12 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Can we hit 9000 likes on dis vidio gujz" };
            ExtraInfo i13 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Megcsalta a feleségét" };
            ExtraInfo i14 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Alkoholista" };
            ExtraInfo i15 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Csukló problémái vannak" };
            ExtraInfo i16 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Alcoholist" };
            ExtraInfo i17 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Alcoholic" };
            ExtraInfo i18 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Ez az info csak azért lett létrehozva hogy bekerüljön a leghosszabb infohoz, Hello =)" };

            this.AddInfoToClient(i0, "test01");
            this.AddInfoToClient(i1, "test01");
            this.AddInfoToClient(i2, "test02");
            this.AddInfoToClient(i3, "test03");
            this.AddInfoToClient(i4, "test04");
            this.AddInfoToClient(i5, "test05");
            this.AddInfoToClient(i6, "test04");
            this.AddInfoToClient(i7, "test03");
            this.AddInfoToClient(i8, "test02");
            this.AddInfoToClient(i9, "test01");
            this.AddInfoToClient(i10, "test06");
            this.AddInfoToClient(i11, "test07");
            this.AddInfoToClient(i12, "test08");
            this.AddInfoToClient(i13, "test09");
            this.AddInfoToClient(i14, "test10");
            this.AddInfoToClient(i15, "test07");
            this.AddInfoToClient(i16, "test08");
            this.AddInfoToClient(i17, "test05");
            this.AddInfoToClient(i18, "test10");
        }
    }
}
