﻿//-----------------------------------------------------------------------
// <copyright file="TrainerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
[assembly: System.CLSCompliant(false)]

namespace Logic
{
    using System;
    using System.Linq;
    using Models;
    using Repos;

    /// <summary>
    /// A class that handles the logic behind the trainer repository.
    /// </summary>
    public class TrainerLogic
    {
        private readonly IRepoBase<Trainer> trainerRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainerLogic"/> class.
        /// </summary>
        /// <param name="trainerRepo">The existing repository.</param>
        public TrainerLogic(IRepoBase<Trainer> trainerRepo)
        {
            this.trainerRepo = trainerRepo;
        }

        /// <summary>
        /// Calls the method in the trainer repository to add a new trainer.
        /// </summary>
        /// <param name="trainer">The trainer who will be added to the repo.</param>
        public void AddTrainer(Trainer trainer)
        {
            this.trainerRepo.Add(trainer);
        }

        /// <summary>
        /// Calls the method in the trainer repository to delete a trainer.
        /// </summary>
        /// <param name="trainerId">The trainer's id who needs to be deleted.</param>
        public void DeleteTrainer(string trainerId)
        {
            this.trainerRepo.Delete(trainerId);
        }

        /// <summary>
        /// Calls the method in the trainer repository to return all trainers.
        /// </summary>
        /// <returns>All the trainers in the trainer repo.</returns>
        public IQueryable<Trainer> GetTrainers()
        {
            return this.trainerRepo.Read();
        }

        /// <summary>
        /// Calls the method in the trainer repository to return a specific trainer.
        /// </summary>
        /// <param name="trainerId">The trainer's id who needs to be found.</param>
        /// <returns>The desired trainer.</returns>
        public Trainer GetTrainer(string trainerId)
        {
            return this.trainerRepo.GetItem(trainerId);
        }

        /// <summary>
        /// Calls the method in the trainer repository to update a specific trainer.
        /// </summary>
        /// <param name="trainerId">The trainer's id who needs to be updated.</param>
        /// <param name="newTrainer">A Trainer object with the new desired properties.</param>
        public void UpdateTrainer(string trainerId, Trainer newTrainer)
        {
            this.trainerRepo.Update(trainerId, newTrainer);
        }

        /// <summary>
        /// A method that returns the average age of all trainers.
        /// </summary>
        /// <returns>The average age of the trainers.</returns>
        public float AverageAge()
        {
            int counter = 0;
            float value = 0;

            var something = this.GetTrainers().ToList();
            foreach (var item in something)
            {
                foreach (var client in item.GymClients)
                {
                    value += client.Age;
                    counter++;
                }
            }

            if (value == 0)
            {
                return 0;
            }
            else
            {
                return value / counter;
            }
        }

        /// <summary>
        /// A method that calculates the gender discrepancy and outputs an int array with all the required information in it.
        /// It stores a value of a certain gender in 1 index so our first index(0) would be woman, second man and third helicopter.
        /// At the end these values also get converted into percentages so we don't have to process anything later.
        /// </summary>
        /// <returns>The occurance of each gender in percentages.</returns>
        public int[] GenderPercentage()
        {
            int[] something = new int[3];
            int counter = 0;
            for (int i = 0; i < something.Length; i++)
            {
                something[i] = 0;
            }

            var helper = this.GetTrainers();

            foreach (var item in helper)
            {
                foreach (var client in item.GymClients)
                {
                    if (client.Gender == Genders.Nő)
                    {
                        something[0]++;
                    }
                    else if (client.Gender == Genders.Férfi)
                    {
                        something[1]++;
                    }
                    else
                    {
                        something[2]++;
                    }

                    counter++;
                }
            }

            for (int i = 0; i < something.Length; i++)
            {
                if (something[i] == 0)
                {
                    something[i] = 0;
                }
                else
                {
                    something[i] = (something[i] * 100) / counter;
                }
            }

            return something;
        }

        /// <summary>
        /// A method that returns the number of trainers in the repository.
        /// </summary>
        /// <returns>Number of trainers.</returns>
        public int AmountOfTrainers()
        {
            int counter = 0;
            var something = this.GetTrainers().ToList();
            foreach (var item in something)
            {
                counter++;
            }

            return counter;
        }

        /// <summary>
        /// A method that returns the number of clients in the repository.
        /// </summary>
        /// <returns>Number of clients.</returns>
        public int AmountOfClients()
        {
            int counter = 0;
            var something = this.GetTrainers().ToList();
            foreach (var item in something)
            {
                foreach (var client in item.GymClients)
                {
                    counter++;
                }
            }

            return counter;
        }

        /// <summary>
        /// A method that returns the number of extrainfo in the repository.
        /// </summary>
        /// <returns>Number of extrainfo.</returns>
        public int AmountOfExtraInfo()
        {
            int counter = 0;
            var something = this.GetTrainers().ToList();
            foreach (var item in something)
            {
                foreach (var client in item.GymClients)
                {
                    foreach (var info in client.ExtraInfos)
                    {
                        counter++;
                    }
                }
            }

            return counter;
        }

        /// <summary>
        /// A method that adds a client to a trainer.
        /// </summary>
        /// <param name="gymClient">The client that needs to be added.</param>
        /// <param name="trainerId">The trainer's id who will get the client.</param>
        public void AddClientToTrainer(GymClient gymClient, string trainerId)
        {
            this.GetTrainer(trainerId).GymClients.Add(gymClient);
            this.trainerRepo.Save();
        }

        /// <summary>
        /// A method that removes a client from the trainer.
        /// </summary>
        /// <param name="gymClient">The client that needs to be removed.</param>
        /// <param name="trainerId">The trainer's id who will get the client removed.</param>
        public void RemoveClientFromTrainer(GymClient gymClient, string trainerId)
        {
            this.GetTrainer(trainerId).GymClients.Remove(gymClient);
            this.trainerRepo.Save();
        }

        /// <summary>
        /// A function that fills the database with samples.
        /// </summary>
        public void FillDbWithSamples()
        {
            Trainer t1 = new () { TrainerName = "Edvás Erezacsi", TrainerID = Guid.NewGuid().ToString() };
            Trainer t2 = new () { TrainerName = "Zacsi Maszíro", TrainerID = Guid.NewGuid().ToString() };
            Trainer t3 = new () { TrainerName = "Medvés Hasnyálmokus", TrainerID = Guid.NewGuid().ToString() };
            /*
            GymClient g0 = new ()
            {
                GymID = Guid.NewGuid().ToString(),
                Gender = Genders.Nő,
                FullName = "Macskás Réka",
                TrainerID = "asd647",
                Verified = false,
                Age = 27,
                BeenWorkingOutFor = 0,
            };
            GymClient g1 = new ()
            {
                GymID = Guid.NewGuid().ToString(),
                Gender = Genders.Nő,
                FullName = "Negnyes Reheracskó",
                TrainerID = "asd647",
                Verified = false,
                Age = 27,
                BeenWorkingOutFor = 0,
            };
            GymClient g2 = new ()
            {
                GymID = "test01",
                Gender = Genders.Férfi,
                FullName = "Komodoros Zacsinyalogato",
                TrainerID = "klo293",
                Verified = false,
                Age = 16,
                BeenWorkingOutFor = 2,
            };
            GymClient g3 = new ()
            {
                GymID = "test02",
                Gender = Genders.Férfi,
                FullName = "Arnold Zacsinegger",
                TrainerID = "huge99",
                Verified = true,
                Age = 33,
                BeenWorkingOutFor = 11,
            };
            GymClient g4 = new ()
            {
                GymID = "test03",
                Gender = Genders.Nő,
                FullName = "Pesti veszettmacska",
                TrainerID = "asd647",
                Verified = true,
                Age = 24,
                BeenWorkingOutFor = 5,
            };
            GymClient g5 = new ()
            {
                GymID = "test04",
                Gender = Genders.Férfi,
                FullName = "obudai kronikuskigyo",
                TrainerID = "asd647",
                Verified = false,
                Age = 12,
                BeenWorkingOutFor = 0,
            };
            GymClient g6 = new ()
            {
                GymID = "test05",
                Gender = Genders.Helikopter,
                FullName = "Jay Duckler",
                TrainerID = "asd647",
                Verified = true,
                Age = 30,
                BeenWorkingOutFor = 10,
            };
            GymClient g7 = new ()
            {
                GymID = "test06",
                Gender = Genders.Férfi,
                FullName = "Ronnie Coalman",
                TrainerID = "asd647",
                Verified = true,
                Age = 35,
                BeenWorkingOutFor = 12,
            };
            GymClient g8 = new ()
            {
                GymID = "test07",
                Gender = Genders.Férfi,
                FullName = "Gizi mama",
                TrainerID = "asd647",
                Verified = false,
                Age = 67,
                BeenWorkingOutFor = 50,
            };
            GymClient g9 = new ()
            {
                GymID = "test08",
                Gender = Genders.Helikopter,
                FullName = "Raid: Shadow Legends",
                TrainerID = "asd647",
                Verified = true,
                Age = 2,
                BeenWorkingOutFor = 0,
            };
            GymClient g10 = new ()
            {
                GymID = "test09",
                Gender = Genders.Helikopter,
                FullName = "Felix shellbörg",
                TrainerID = "asd647",
                Verified = false,
                Age = 24,
                BeenWorkingOutFor = 2,
            };
            GymClient g11 = new ()
            {
                GymID = "test10",
                Gender = Genders.Helikopter,
                FullName = "Fill Heath",
                TrainerID = "asd647",
                Verified = true,
                Age = 17,
                BeenWorkingOutFor = 2,
            };
            */
            this.AddTrainer(t1);
            this.AddTrainer(t2);
            this.AddTrainer(t3);

            /*
            this.AddClientToTrainer(g0, t1.TrainerID);
            this.AddClientToTrainer(g1, t1.TrainerID);
            this.AddClientToTrainer(g2, t2.TrainerID);
            this.AddClientToTrainer(g3, t3.TrainerID);
            this.AddClientToTrainer(g4, t3.TrainerID);
            this.AddClientToTrainer(g5, t2.TrainerID);
            this.AddClientToTrainer(g6, t2.TrainerID);
            this.AddClientToTrainer(g7, t1.TrainerID);
            this.AddClientToTrainer(g8, t1.TrainerID);
            this.AddClientToTrainer(g9, t2.TrainerID);
            this.AddClientToTrainer(g10, t3.TrainerID);
            this.AddClientToTrainer(g11, t1.TrainerID);
            */
        }
    }
}
