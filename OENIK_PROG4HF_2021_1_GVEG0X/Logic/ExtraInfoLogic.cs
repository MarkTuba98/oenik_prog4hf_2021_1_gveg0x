﻿//-----------------------------------------------------------------------
// <copyright file="ExtraInfoLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Logic
{
    using System.Linq;
    using Models;
    using Repos;

    /// <summary>
    /// A class that handles the logic behind the ExtraInfo repository.
    /// </summary>
    public class ExtraInfoLogic
    {
        private readonly IRepoBase<ExtraInfo> infoRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraInfoLogic"/> class.
        /// </summary>
        /// <param name="infoRepo">The existing repository.</param>
        public ExtraInfoLogic(IRepoBase<ExtraInfo> infoRepo)
        {
            this.infoRepo = infoRepo;
        }

        /// <summary>
        /// Adds a new ExtraInfo into the info Repository.
        /// </summary>
        /// <param name="extraInfo">The ExtraInfo to add.</param>
        public void AddInfo(ExtraInfo extraInfo)
        {
            this.infoRepo.Add(extraInfo);
        }

        /// <summary>
        /// Deletes a new ExtraInfo from the info Repository.
        /// </summary>
        /// <param name="infoId">The ExtraInfo's id that needs to be deleted.</param>
        public void DeleteInfo(string infoId)
        {
            this.infoRepo.Delete(infoId);
        }

        /// <summary>
        /// Calls the method in the info repository to get all extrainfo.
        /// </summary>
        /// <returns>All the extrainfo in the info repo.</returns>
        public IQueryable<ExtraInfo> GetInfos()
        {
            return this.infoRepo.Read();
        }

        /// <summary>
        /// Calls the method in the info repository to return a specific extrainfo.
        /// </summary>
        /// <param name="infoId">The extrainfo's id who needs to be found.</param>
        /// <returns>The desired info.</returns>
        public ExtraInfo GetInfo(string infoId)
        {
            return this.infoRepo.GetItem(infoId);
        }

        /// <summary>
        /// Calls the method in the info repository to update a specific extrainfo's properties.
        /// </summary>
        /// <param name="infoId">The extrainfo's id who needs to be updated.</param>
        /// <param name="extraInfo">A ExtraInfo object with the new desired properties.</param>
        public void UpdateInfo(string infoId, ExtraInfo extraInfo)
        {
            this.infoRepo.Update(infoId, extraInfo);
        }
    }
}
