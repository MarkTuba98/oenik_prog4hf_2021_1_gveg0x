﻿//-----------------------------------------------------------------------
// <copyright file="EditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp.UI
{
    using System.Windows;
    using Models;
    using WpfApp.VM;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class EditorWindow : Window
    {
        private EditorViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        public EditorWindow()
        {
            this.InitializeComponent();

            this.viewModel = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        /// <param name="oldTrainer">The old trainer.</param>
        public EditorWindow(Trainer oldTrainer)
            : this()
        {
            this.viewModel.Trainer = oldTrainer;
        }

        /// <summary>
        /// Gets trainer from the viewmodel.
        /// </summary>
        public Trainer Trainer { get => this.viewModel.Trainer; }

        /// <summary>
        /// If the click is ok do this.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">Event arguments.</param>
        private void OkClick(object sender, RoutedEventArgs eventArgs)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// If the click is cancelled do this.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">Event arguments.</param>
        private void CancelClick(object sender, RoutedEventArgs eventArgs)
        {
            this.DialogResult = false;
        }
    }
}
