﻿//-----------------------------------------------------------------------
// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp.UI
{
    using Models;
    using WpfApp.BL;

    /// <summary>
    /// Editor service via Window.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// Edit trainer.
        /// </summary>
        /// <param name="trainer">The trainer you wish to edit.</param>
        /// <returns>True or false.</returns>
        public bool EditTrainer(Trainer trainer)
        {
            EditorWindow win = new (trainer);
            return win.ShowDialog() ?? false;
        }
    }
}
