﻿//-----------------------------------------------------------------------
// <copyright file="TestDataGeneration.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp.DataGeneration
{
    using System;
    using Logic;
    using Models;

    /// <summary>
    /// A class whose only purpose is to generate test data.
    /// </summary>
    public class TestDataGeneration
    {
        private TrainerLogic trainerLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDataGeneration"/> class.
        /// </summary>
        /// <param name="trainerLogic">Trainer logic.</param>
        public TestDataGeneration(TrainerLogic trainerLogic)
        {
            this.trainerLogic = trainerLogic;
        }

        /// <summary>
        /// Fills the database with samples.
        /// </summary>
        public void FillDbWithSamples()
        {
            Trainer t1 = new () { TrainerName = "Edvás Erezacsi", TrainerID = Guid.NewGuid().ToString() };
            Trainer t2 = new () { TrainerName = "Zacsi Maszíro", TrainerID = Guid.NewGuid().ToString() };
            Trainer t3 = new () { TrainerName = "Medvés Hasnyálmokus", TrainerID = Guid.NewGuid().ToString() };
            this.trainerLogic.AddTrainer(t1);
            this.trainerLogic.AddTrainer(t2);
            this.trainerLogic.AddTrainer(t3);
        }
    }
}
