﻿//-----------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp
{
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Messaging;
    using WpfApp.BL;
    using WpfApp.DataGeneration;
    using WpfApp.UI;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        private TestDataGeneration dataGeneration = new (new Logic.TrainerLogic(new Repos.TrainerRepository()));

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);

            MyIoc.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoc.Instance.Register<IUITrainerLogic, UITrainerLogic>();
            MyIoc.Instance.Register<Logic.TrainerLogic>();
            MyIoc.Instance.Register<Repos.IRepoBase<Models.Trainer>, Repos.TrainerRepository>();

            this.dataGeneration.FillDbWithSamples();
        }
    }
}
