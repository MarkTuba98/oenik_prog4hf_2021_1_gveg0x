﻿//-----------------------------------------------------------------------
// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp.BL
{
    using Models;

    /// <summary>
    /// An interface for EditorService.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// A method that send back true or false whenever we are editing a Trainer.
        /// </summary>
        /// <param name="trainer">The trainer you wish to edit.</param>
        /// <returns>true or false.</returns>
        bool EditTrainer(Trainer trainer);
    }
}
