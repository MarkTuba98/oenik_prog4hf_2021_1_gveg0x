﻿//-----------------------------------------------------------------------
// <copyright file="IUITrainerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp.BL
{
    using System.Collections.Generic;
    using Models;

    /// <summary>
    /// An interface for our UI logic.
    /// </summary>
    public interface IUITrainerLogic
    {
        /// <summary>
        /// A method that lets us add trainers.
        /// </summary>
        /// <param name="trainers">The trainer(s) you wish to add.</param>
        void AddTrainer(IList<Trainer> trainers);

        /// <summary>
        /// A method that lets us delete a trainer.
        /// </summary>
        /// <param name="list">The list you wish to delete from.</param>
        /// <param name="trainer">The trainer you wish to delete.</param>
        void DelTrainer(IList<Trainer> list, Trainer trainer);

        /// <summary>
        /// A method that returns all trainers as an IList.
        /// </summary>
        /// <returns>A list containing all trainers.</returns>
        IList<Trainer> GetAllTrainers();

        /// <summary>
        /// A method that allows us to modify a Trainer.
        /// </summary>
        /// <param name="trainer">The trainer you wish to modify.</param>
        void ModTrainer(Trainer trainer);
    }
}