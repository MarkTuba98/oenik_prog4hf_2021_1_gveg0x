﻿//-----------------------------------------------------------------------
// <copyright file="UITrainerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Logic;
    using Models;

    /// <summary>
    /// The implementation of the UI logic.
    /// </summary>
    public class UITrainerLogic : IUITrainerLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private TrainerLogic trainerLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="UITrainerLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editor service.</param>
        /// <param name="messengerService">Messenger service.</param>
        /// <param name="trainerLogic">Trainer logic.</param>
        public UITrainerLogic(IEditorService editorService, IMessenger messengerService, TrainerLogic trainerLogic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.trainerLogic = trainerLogic;
        }

        /// <summary>
        /// Adds trainer.
        /// </summary>
        /// <param name="trainers">The trainer you wish to add.</param>
        public void AddTrainer(IList<Trainer> trainers)
        {
            Trainer newTrainer = new ();
            if (this.editorService.EditTrainer(newTrainer) && trainers != null)
            {
                newTrainer.TrainerID = Guid.NewGuid().ToString();

                trainers.Add(newTrainer);
                this.trainerLogic.AddTrainer(newTrainer);
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Deletes trainer.
        /// </summary>
        /// <param name="list">The list you wish to delete from.</param>
        /// <param name="trainer">The trainer you wish to delete.</param>
        public void DelTrainer(IList<Trainer> list, Trainer trainer)
        {
            if (trainer != null && list != null && list.Remove(trainer))
            {
                this.trainerLogic.DeleteTrainer(trainer.TrainerID);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// Gets all trainers.
        /// </summary>
        /// <returns>Returns a list of all trainers.</returns>
        public IList<Trainer> GetAllTrainers()
        {
            return this.trainerLogic.GetTrainers().ToList();
        }

        /// <summary>
        /// Modifies a trainer.
        /// </summary>
        /// <param name="trainer">The trainer you wish to delete.</param>
        public void ModTrainer(Trainer trainer)
        {
            if (trainer == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            Trainer clone = new ();
            clone.CopyFrom(trainer);

            if (this.editorService.EditTrainer(clone))
            {
                trainer.CopyFrom(clone);
                this.trainerLogic.UpdateTrainer(trainer.TrainerID, clone);
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCEL", "LogicResult");
            }
        }
    }
}
