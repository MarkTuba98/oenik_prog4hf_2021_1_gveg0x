﻿//-----------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using WpfApp.BL;

    /// <summary>
    /// A class for our Main view model.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IUITrainerLogic trainerLogic;
        private Trainer selectedTrainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IUITrainerLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="trainerLogic">The UI Trainer logic.</param>
        public MainViewModel(IUITrainerLogic trainerLogic)
        {
            this.trainerLogic = trainerLogic;

            if (this.IsInDesignMode)
            {
                Trainer t1 = new () { TrainerName = "Edvás Erezacsi", TrainerID = Guid.NewGuid().ToString() };
                Trainer t2 = new () { TrainerName = "Zacsi Maszíro", TrainerID = Guid.NewGuid().ToString() };

                this.Trainers = new ObservableCollection<Trainer>
                {
                    t1,
                    t2,
                };
            }
            else
            {
                this.Trainers = new ObservableCollection<Trainer>(this.trainerLogic.GetAllTrainers());
            }

            this.AddCmd = new RelayCommand(() => this.trainerLogic.AddTrainer(this.Trainers));
            this.DelCmd = new RelayCommand(() => this.trainerLogic.DelTrainer(this.Trainers, this.SelectedTrainer));
            this.ModCmd = new RelayCommand(() => this.trainerLogic.ModTrainer(this.SelectedTrainer));
        }

        /// <summary>
        /// Gets observable collection of the trainers.
        /// </summary>
        public ObservableCollection<Trainer> Trainers { get; private set; }

        /// <summary>
        /// Gets or sets the currently selected trainer.
        /// </summary>
        public Trainer SelectedTrainer
        {
            get { return this.selectedTrainer; }
            set { this.Set(ref this.selectedTrainer, value); }
        }

        /// <summary>
        /// Gets the add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets the modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }
    }
}
