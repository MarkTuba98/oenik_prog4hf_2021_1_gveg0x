﻿//-----------------------------------------------------------------------
// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfApp.VM
{
    using GalaSoft.MvvmLight;
    using Models;

    /// <summary>
    /// Editor view model.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private Trainer trainer;

        /// <summary>
        /// Gets or sets the Trainer field.
        /// </summary>
        public Trainer Trainer
        {
            get { return this.trainer; }
            set { this.Set(ref this.trainer, value); }
        }
    }
}
