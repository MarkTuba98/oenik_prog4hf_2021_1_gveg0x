﻿//-----------------------------------------------------------------------
// <copyright file="Statistics.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Models
{
    /// <summary>
    /// A class for statistical data on our clients, that will be displayed on the website.
    /// Contains all required properties.
    /// </summary>
    public class Statistics
    {
        /// <summary>
        /// Rrivate field for the gender array.
        /// </summary>
        private int[] genderPercentage;

        /// <summary>
        /// Gets or sets the average age of all clients.
        /// </summary>
        public float AverageAgeValue { get; set; }

        /// <summary>
        /// Gets or sets the total number of trainers there are.
        /// </summary>
        public int AmountOfTrainers { get; set; }

        /// <summary>
        /// Gets or sets the total number of clients there are.
        /// </summary>
        public int AmountOfClients { get; set; }

        /// <summary>
        /// Gets or sets the total number of information there are about all clients.
        /// </summary>
        public int AmountOfExtraInfo { get; set; }

        /// <summary>
        /// Gets or sets the total number of alcoholics there are.
        /// </summary>
        public int AmountOfAlcoholists { get; set; }

        /// <summary>
        /// Gets or sets the longest information that any client has.
        /// </summary>
        public string LongestInfo { get; set; }

        /// <summary>
        /// Gets the discrepancy between the different genders.
        /// </summary>
        /// <returns>Returns the gender array.</returns>
        public int[] GetGenderPercentage()
        {
            return this.genderPercentage;
        }

        /// <summary>
        /// Sets the discrepancy between the different genders.
        /// </summary>
        /// <param name="value">The desired value of the array.</param>
        public void SetGenderPercentage(int[] value)
        {
            this.genderPercentage = value;
        }
    }
}
