﻿//-----------------------------------------------------------------------
// <copyright file="ExtraInfo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// A class that allows us to have more information about our clients.
    /// Contains all required properties.
    /// </summary>
    public class ExtraInfo
    {
        /// <summary>
        /// Gets or sets the information's id.
        /// </summary>
        [Key]
        public string InfoId { get; set; }

        /// <summary>
        /// Gets or sets the information.
        /// </summary>
        [StringLength(200)]
        public string Information { get; set; }

        /// <summary>
        /// Gets or sets the information's "owner", in this case the GymClient's id.
        /// </summary>
        public string GymID { get; set; }

        /// <summary>
        /// Gets or sets the information's "owner", in this case the GymClient's virtual property.
        /// </summary>
        [NotMapped]
        public virtual GymClient GymClient { get; set; }
    }
}
