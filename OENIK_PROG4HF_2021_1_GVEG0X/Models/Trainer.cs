﻿//-----------------------------------------------------------------------
// <copyright file="Trainer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
[assembly: System.CLSCompliant(false)]

namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    /// <summary>
    /// A class for the trainers.
    /// Contains the constructor and all required properties.
    /// </summary>
    public class Trainer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Trainer"/> class.
        /// </summary>
        public Trainer()
        {
            this.GymClients = new List<GymClient>();
        }

        /// <summary>
        /// Gets or sets the trainer's id.
        /// </summary>
        [Key]
        public string TrainerID { get; set; }

        /// <summary>
        /// Gets or sets the trainer's name.
        /// </summary>
        [StringLength(200)]
        public string TrainerName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the trainer is selected or not.
        /// </summary>
        public bool Selected { get; set; }

        /// <summary>
        /// Gets the gymclients that the trainer trains.
        /// </summary>
        public virtual ICollection<GymClient> GymClients { get; private set; }

        /// <summary>
        /// A method that copies the properties of a Trainer object onto another one.
        /// </summary>
        /// <param name="otherTrainer">The trainer you wish to inherit from.</param>
        public void CopyFrom(Trainer otherTrainer)
        {
            if (otherTrainer != null)
            {
                this.TrainerID = otherTrainer.TrainerID;
                this.TrainerName = otherTrainer.TrainerName;
            }
        }
    }
}
