﻿//-----------------------------------------------------------------------
// <copyright file="GymClient.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Types of genders.
    /// </summary>
    public enum Genders
    {
        /// <summary>
        /// Represents a woman.
        /// </summary>
        Nő,

        /// <summary>
        /// Represents a man.
        /// </summary>
        Férfi,

        /// <summary>
        /// Represents a helicopter(obviously).
        /// </summary>
        Helikopter,
    }

    /// <summary>
    /// A class for the clients.
    /// Contains all required properties and a ToString method.
    /// </summary>
    public class GymClient
    {
        /// <summary>
        /// Gets or sets the client's id.
        /// </summary>
        [Key]
        public string GymID { get; set; }

        /// <summary>
        /// Gets or sets the client's full name.
        /// </summary>
        [StringLength(200)]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the client's age.
        /// </summary>
        [Range(14, 100)]
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets the client's gender.
        /// </summary>
        public Genders Gender { get; set; }

        /// <summary>
        /// Gets or sets how long the client has been working out for.
        /// </summary>
        [Range(0, 86)]
        public int BeenWorkingOutFor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the client competes in bodybuilding or not.
        /// </summary>
        public bool Verified { get; set; }

        /// <summary>
        /// Gets or sets the client's trainer's id.
        /// </summary>
        public string TrainerID { get; set; }

        /// <summary>
        /// Gets or sets the client's trainer's virtual property.
        /// </summary>
        [NotMapped]
        public virtual Trainer Trainer { get; set; }

        /// <summary>
        /// Gets the client's additional information.
        /// </summary>
        public virtual ICollection<ExtraInfo> ExtraInfos { get; }

        /// <summary>
        /// A method that combines all properties into a single string.
        /// </summary>
        /// <returns>Combined string.</returns>
        public override string ToString()
        {
            return $"{this.GymID}-{this.FullName}-{this.Age}-{this.Gender}-{this.BeenWorkingOutFor}-{this.Verified}";
        }
    }
}