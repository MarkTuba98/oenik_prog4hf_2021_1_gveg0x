﻿//-----------------------------------------------------------------------
// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Net.Http;
    using System.Text.Json;
    using GalaSoft.MvvmLight.Messaging;
    using WpfClient.VM;

    /// <summary>
    /// Main Logic.
    /// </summary>
    public class MainLogic
    {
        private string url = "http://localhost:5000/TrainerApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// SendMessage.
        /// </summary>
        /// <param name="success">Bool.</param>
        public void SendMessage(bool success)
        {
            string msg = success ? "success" : "failed";
            Messenger.Default.Send(msg, "TrainerResult");
        }

        /// <summary>
        /// Api Get.
        /// </summary>
        /// <returns>List.</returns>
        public Collection<TrainerVM> ApiGetTrainers()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<Collection<TrainerVM>>(json, this.jsonOptions);

            return list;
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="model">model.</param>
        public void ApiDelTrainer(TrainerVM model)
        {
            bool success = false;
            if (model != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + model.TrainerID).Result;
                JsonDocument doc = JsonDocument.Parse(json);

                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// Bruh.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="isEditing">Bool.</param>
        /// <returns>Returns Bool.</returns>
        public bool ApiEditTrainer(TrainerVM model, bool isEditing)
        {
            if (model == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("TrainerID", model.TrainerID);
            }

            postData.Add("TrainerName", model.TrainerName);

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);

            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Edit trainer.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="editorFunc">EditorFunc.</param>
        public void EditTrainer(TrainerVM model, Func<TrainerVM, bool> editorFunc)
        {
            TrainerVM clone = new ();
            if (model != null)
            {
                clone.CopyFrom(model);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (model != null)
                {
                    success = this.ApiEditTrainer(clone, true);
                }
                else
                {
                    success = this.ApiEditTrainer(clone, false);
                }
            }

            this.SendMessage(success == true);
        }
    }
}
