﻿//-----------------------------------------------------------------------
// <copyright file="EditorView.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Models;
    using WpfClient.VM;

    /// <summary>
    /// Interaction logic for EditorView.xaml.
    /// </summary>
    public partial class EditorView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditorView"/> class.
        /// </summary>
        public EditorView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorView"/> class.
        /// </summary>
        /// <param name="trainer">trainer.</param>
        public EditorView(TrainerVM trainer)
            : this()
        {
            this.DataContext = trainer;
        }

        /// <summary>
        /// If the click is ok do this.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">Event arguments.</param>
        private void OkClick(object sender, RoutedEventArgs eventArgs)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// If the click is cancelled do this.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">Event arguments.</param>
        private void CancelClick(object sender, RoutedEventArgs eventArgs)
        {
            this.DialogResult = false;
        }
    }
}
