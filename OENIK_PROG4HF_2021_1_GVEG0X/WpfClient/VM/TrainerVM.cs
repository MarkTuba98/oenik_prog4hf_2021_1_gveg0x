﻿//-----------------------------------------------------------------------
// <copyright file="TrainerVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfClient.VM
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Trainer view model.
    /// </summary>
    public class TrainerVM : ObservableObject
    {
        private string trainerID;
        private string trainerName;

        /// <summary>
        /// Gets or sets the trainer's id.
        /// </summary>
        public string TrainerID
        {
            get { return this.trainerID; }
            set { this.Set(ref this.trainerID, value); }
        }

        /// <summary>
        /// Gets or sets the trainer's name.
        /// </summary>
        public string TrainerName
        {
            get { return this.trainerName; }
            set { this.Set(ref this.trainerName, value); }
        }

        /// <summary>
        /// Copy from other vm.
        /// </summary>
        /// <param name="other">TrainerVm.</param>
        public void CopyFrom(TrainerVM other)
        {
            if (other is null)
            {
                return;
            }

            this.TrainerID = other.TrainerID;
            this.TrainerName = other.TrainerName;
        }
    }
}
