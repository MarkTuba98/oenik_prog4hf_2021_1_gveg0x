﻿//-----------------------------------------------------------------------
// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfClient.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using Models;

    /// <summary>
    /// Editor View Model.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private Trainer trainer;

        /// <summary>
        /// Gets or sets the Trainer field.
        /// </summary>
        public Trainer Trainer
        {
            get { return this.trainer; }
            set { this.Set(ref this.trainer, value); }
        }
    }
}
