﻿//-----------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfClient.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Class.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private MainLogic logic;
        private TrainerVM selectedTrainer;
        private ObservableCollection<TrainerVM> allTrainers;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            this.logic = new MainLogic(); // IoC + Dependency.

            this.LoadCmd = new RelayCommand(() =>
            this.Trainers = new ObservableCollection<TrainerVM>(this.logic.ApiGetTrainers()));

            this.AddCmd = new RelayCommand(() => this.logic.EditTrainer(null, this.EditorFunc));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelTrainer(this.SelectedTrainer));
            this.ModCmd = new RelayCommand(() => this.logic.EditTrainer(this.SelectedTrainer, this.EditorFunc));
        }

        /// <summary>
        /// Gets or sets all trainers.
        /// </summary>
        public ObservableCollection<TrainerVM> Trainers
        {
            get { return this.allTrainers; }
            set { this.Set(ref this.allTrainers, value); }
        }

        /// <summary>
        /// Gets or sets the selected trainer.
        /// </summary>
        public TrainerVM SelectedTrainer
        {
            get { return this.selectedTrainer; }
            set { this.Set(ref this.selectedTrainer, value); }
        }

        /// <summary>
        /// Gets or sets the editor func.
        /// </summary>
        public Func<TrainerVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets the add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets the modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets the load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
