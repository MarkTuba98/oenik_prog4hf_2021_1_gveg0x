﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "Yes", Scope = "type", Target = "~T:WpfClient.MainLogic")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Yes", Scope = "member", Target = "~M:WpfClient.MainLogic.SendMessage(System.Boolean)")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "Yes", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiEditTrainer(WpfClient.EditorViewModel,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Yes", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiDelTrainer(WpfClient.EditorViewModel)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Yes", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiEditTrainer(WpfClient.EditorViewModel,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Yes", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiGetTrainers~System.Collections.ObjectModel.Collection{WpfClient.EditorViewModel}")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Yes", Scope = "member", Target = "~P:WpfClient.MainViewModel.Trainers")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Reviewed", Scope = "member", Target = "~P:WpfClient.VM.MainViewModel.Trainers")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiGetTrainers~System.Collections.ObjectModel.Collection{WpfClient.VM.TrainerVM}")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiDelTrainer(WpfClient.VM.TrainerVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiEditTrainer(WpfClient.VM.TrainerVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfClient.MainLogic.ApiEditTrainer(WpfClient.VM.TrainerVM,System.Boolean)~System.Boolean")]
