//-----------------------------------------------------------------------
// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WebApplication
{
    using AutoMapper;
    using Logic;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Models;
    using Repos;
    using WebApplication.Model;

    /// <summary>
    /// A class where we configure various things about the database.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// A method that configures the services.
        /// </summary>
        /// <param name="services">The service collection we wish to add.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc((opt) => opt.EnableEndpointRouting = false);
            services.AddTransient<TrainerLogic, TrainerLogic>();
            services.AddTransient<ClientLogic, ClientLogic>();
            services.AddTransient<ExtraInfoLogic, ExtraInfoLogic>();

            services.AddTransient<IRepoBase<GymClient>, ClientRepository>();
            services.AddTransient<IRepoBase<Models.Trainer>, TrainerRepository>();
            services.AddTransient<IRepoBase<ExtraInfo>, InfoRepository>();
            services.AddSingleton<IMapper>(provider => MapperFactory.CreateMapper());
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The app.</param>
        /// <param name="env">The env.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
