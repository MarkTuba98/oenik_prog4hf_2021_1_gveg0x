﻿//-----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "<Reviewed>", Scope = "member", Target = "~M:WebApplication.Controllers.RandomController.GenerateName~System.String")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Reviewed>", Scope = "member", Target = "~M:WebApplication.Controllers.RandomController.GenerateName~System.String")]
