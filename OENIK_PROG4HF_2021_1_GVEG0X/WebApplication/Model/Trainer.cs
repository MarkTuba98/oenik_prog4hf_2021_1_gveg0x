﻿//-----------------------------------------------------------------------
// <copyright file="Trainer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WebApplication.Model
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Form Model.
    /// </summary>
    public class Trainer
    {
        /// <summary>
        /// Gets or sets the trainer's id.
        /// </summary>
        [Display(Name = "Trainer Id")]
        [Required]
        public string TrainerID { get; set; }

        /// <summary>
        /// Gets or sets the trainer's name.
        /// </summary>
        [Display(Name = "Trainer Name")]
        [Required]
        public string TrainerName { get; set; }
    }
}
