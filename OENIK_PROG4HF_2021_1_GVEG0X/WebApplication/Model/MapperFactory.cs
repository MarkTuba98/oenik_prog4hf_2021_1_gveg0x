﻿//-----------------------------------------------------------------------
// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WebApplication.Model
{
    using AutoMapper;

    /// <summary>
    /// MapperFactory.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Create mapper.
        /// </summary>
        /// <returns>IMapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Models.Trainer, Model.Trainer>().ForMember(dest => dest.TrainerID, map => map.MapFrom(src => src.TrainerID)).
                ForMember(dest => dest.TrainerName, map => map.MapFrom(src => src.TrainerName));
            });
            return config.CreateMapper();
        }
    }
}
