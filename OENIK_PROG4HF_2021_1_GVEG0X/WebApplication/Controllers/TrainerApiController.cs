﻿//-----------------------------------------------------------------------
// <copyright file="TrainerApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WebApplication.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Logic;
    using Microsoft.AspNetCore.Mvc;
    using Models;

    /// <summary>
    /// Trainer Api Controller.
    /// </summary>
    public class TrainerApiController : Controller
    {
        private TrainerLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainerApiController"/> class.
        /// </summary>
        /// <param name="logic">Trainerlogic.</param>
        /// <param name="mapper">AutoMapper.</param>
        public TrainerApiController(TrainerLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// GetAllTrainers.
        /// </summary>
        /// <returns>List of Trainers.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Model.Trainer> GetAllTrainers()
        {
            var trainers = this.logic.GetTrainers();
            return this.mapper.Map<IList<Models.Trainer>, List<Model.Trainer>>(trainers.ToList());
        }

        /// <summary>
        /// Adds new trainer.
        /// </summary>
        /// <param name="trainer">Trainer.</param>
        /// <returns>Success or failed.</returns>
        [HttpPost]
        [ActionName("add")]
        public Model.ApiResult AddTrainer(Model.Trainer trainer)
        {
            if (trainer is null)
            {
                return new Model.ApiResult() { OperationResult = false };
            }

            bool success = true;

            try
            {
                Models.Trainer helper = new Trainer() { TrainerID = System.Guid.NewGuid().ToString(), TrainerName = trainer.TrainerName };
                this.logic.AddTrainer(helper);
            }
            catch (System.ArgumentException)
            {
                success = false;
            }

            return new Model.ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// DeOneCategory for delete one cateory.
        /// </summary>
        /// <param name="id">Stirng type parameter.</param>
        /// <returns>Return an ApiResult type.</returns>
        [HttpGet]
        [ActionName("del")]
        public Model.ApiResult DeleteT(string id)
        {
            if (this.logic.GetTrainer(id) != null)
            {
                this.logic.DeleteTrainer(id);
                return new Model.ApiResult() { OperationResult = true };
            }
            else
            {
                return new Model.ApiResult() { OperationResult = false };
            }
        }

        /// <summary>
        /// Updates a trainerr.
        /// </summary>
        /// <param name="trainer">Trainer.</param>
        /// <returns>Success or failed.</returns>
        [HttpPost]
        [ActionName("mod")]
        public Model.ApiResult UpdateTrainer(Model.Trainer trainer)
        {
            if (trainer is null)
            {
                return new Model.ApiResult() { OperationResult = false };
            }

            bool success = true;

            try
            {
                Models.Trainer helper = new () { TrainerID = trainer.TrainerID, TrainerName = trainer.TrainerName };
                this.logic.UpdateTrainer(helper.TrainerID, helper);
            }
            catch (System.ArgumentException)
            {
                success = false;
            }

            return new Model.ApiResult() { OperationResult = success };
        }
    }
}
