﻿//-----------------------------------------------------------------------
// <copyright file="RandomController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WebApplication.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Logic;
    using Microsoft.AspNetCore.Mvc;
    using Models;

    /// <summary>
    /// A Randomizer Controller.
    /// </summary>
    public class RandomController : Controller
    {
        private TrainerLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomController"/> class.
        /// </summary>
        /// <param name="logic">TrainerLogic.</param>
        public RandomController(TrainerLogic logic)
        {
            this.logic = logic;
        }

        /// <summary>
        /// Creates a random Trainer and adds it to the repo.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [ActionName("GetOne")]
        public IActionResult GetOne()
        {
            Trainer helper = new () { TrainerID = Guid.NewGuid().ToString(), TrainerName = this.GenerateName() };
            this.logic.AddTrainer(helper);
            return this.Json(helper);
        }

        /// <summary>
        /// Select a trainer.
        /// </summary>
        /// <param name="id">ID of trainer.</param>
        /// <returns>Json result.</returns>
        [HttpGet]
        public IActionResult Select(string id)
        {
            bool success = true;

            Models.Trainer trainer = this.logic.GetTrainer(id);

            if (trainer != null)
            {
                trainer.Selected = true;
            }

            try
            {
                this.logic.UpdateTrainer(trainer.TrainerID, trainer);
            }
            catch (System.ArgumentException)
            {
                success = false;
            }

            return this.Json(success);
        }

        /// <summary>
        /// Unselect a trainer.
        /// </summary>
        /// <param name="id">ID of trainer.</param>
        /// <returns>Json result.</returns>
        [HttpGet]
        public IActionResult Unselect(string id)
        {
            bool success = true;

            Models.Trainer trainer = this.logic.GetTrainer(id);

            if (trainer != null)
            {
                trainer.Selected = false;
            }

            try
            {
                this.logic.UpdateTrainer(trainer.TrainerID, trainer);
            }
            catch (System.ArgumentException)
            {
                success = false;
            }

            return this.Json(success);
        }

        /// <summary>
        /// Selections list.
        /// </summary>
        /// <returns>Json result.</returns>
        [HttpGet]
        public IActionResult Selections()
        {
            var helper = this.logic.GetTrainers();
            List<List<string>> returnArray = new List<List<string>>();
            List<string> selected = new ();
            List<string> unselected = new ();

            foreach (var item in helper)
            {
                if (item.Selected)
                {
                    selected.Add(item.TrainerName);
                }
                else
                {
                    unselected.Add(item.TrainerName);
                }
            }

            if (selected.Count == 0)
            {
                selected.Add("Empty");
            }

            if (unselected.Count == 0)
            {
                unselected.Add("Empty");
            }

            returnArray.Add(selected);
            returnArray.Add(unselected);

            return this.View(returnArray);
        }

        private string GenerateName()
        {
            Random r = new ();
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "ae", "y" };
            string helper = string.Empty;

            helper += consonants[r.Next(consonants.Length)].ToUpper(new CultureInfo("en-US", false));
            helper += vowels[r.Next(vowels.Length)];
            int b = 2;
            while (b < r.Next(4, 11))
            {
                helper += consonants[r.Next(consonants.Length)];
                b++;
                helper += vowels[r.Next(vowels.Length)];
                b++;
            }

            helper += " ";

            helper += consonants[r.Next(consonants.Length)].ToUpper(new CultureInfo("en-US", false));
            helper += vowels[r.Next(vowels.Length)];
            b = 2;
            while (b < r.Next(3, 11))
            {
                helper += consonants[r.Next(consonants.Length)];
                b++;
                helper += vowels[r.Next(vowels.Length)];
                b++;
            }

            return helper;
        }
    }
}
