﻿//-----------------------------------------------------------------------
// <copyright file="HomeController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WebApplication.Controllers
{
    using System;
    using System.Linq;
    using Logic;
    using Microsoft.AspNetCore.Mvc;
    using Models;

    /// <summary>
    /// A class that is the main controller for our web app.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ClientLogic clientLogic;
        private readonly ExtraInfoLogic infoLogic;
        private readonly TrainerLogic trainerLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="trainerLogic">The trainer repository.</param>
        /// <param name="clientLogic">The client repository.</param>
        /// <param name="infoLogic">The info repository.</param>
        public HomeController(TrainerLogic trainerLogic, ClientLogic clientLogic, ExtraInfoLogic infoLogic)
        {
            this.trainerLogic = trainerLogic;
            this.clientLogic = clientLogic;
            this.infoLogic = infoLogic;
        }

        /// <summary>
        /// Our home page.
        /// </summary>
        /// <returns>Redirects us to Index.</returns>
        public IActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// Fills our database with samples and returns us to Index.
        /// </summary>
        /// <returns>Redirects us to Index.</returns>
        public IActionResult Init()
        {
            this.trainerLogic.FillDbWithSamples();

            // this.clientLogic.FillDbWithSamples();
            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Lists the statistics about the gym's clients, trainers and extrainfos.
        /// </summary>
        /// <returns>Redirects us to Statistics.</returns>
        [HttpGet]
        public IActionResult Statistics()
        {
            Statistics statistics = new ()
            {
                AverageAgeValue = this.trainerLogic.AverageAge(),
                AmountOfTrainers = this.trainerLogic.AmountOfTrainers(),
                AmountOfClients = this.trainerLogic.AmountOfClients(),
                AmountOfExtraInfo = this.trainerLogic.AmountOfExtraInfo(),
                AmountOfAlcoholists = this.clientLogic.AmountOfAlcoholists(),
                LongestInfo = this.clientLogic.LongestInfo(),
            };
            statistics.SetGenderPercentage(this.trainerLogic.GenderPercentage());
            return this.View(nameof(this.Statistics), statistics);
        }

        /// <summary>
        /// Sends a Get request to create a client for a specific trainer.
        /// </summary>
        /// <param name="trainerId">The trainer's id who will get the client.</param>
        /// <returns>Redirects us to CreateClient.</returns>
        [HttpGet]
        public IActionResult CreateClient(string trainerId)
        {
            return this.View(nameof(this.CreateClient), trainerId);
        }

        /// <summary>
        /// Creates the new client and adds it to the trainer's clients.
        /// </summary>
        /// <param name="trainerId">The trainer's id who will get the client.</param>
        /// <param name="gymClient">The desired client's data.</param>
        /// <returns>Redirects us to GetTrainer.</returns>
        [HttpPost]
        public IActionResult CreateClient(string trainerId, GymClient gymClient)
        {
            if (gymClient != null)
            {
                gymClient.GymID = Guid.NewGuid().ToString();
                gymClient.TrainerID = trainerId;
                this.trainerLogic.AddClientToTrainer(gymClient, trainerId);
                return this.RedirectToAction(nameof(this.GetTrainer), new { trainerId });
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Sends a Get request to create an ExtraInfo for a specific client.
        /// </summary>
        /// <param name="clientId">The client's id who will get the extrainfo.</param>
        /// <returns>Redirects us to CreateInfo.</returns>
        [HttpGet]
        public IActionResult CreateInfo(string clientId)
        {
            return this.View(nameof(this.CreateInfo), clientId);
        }

        /// <summary>
        /// Creates the new extrainfo and adds it to the trainer's infos.
        /// </summary>
        /// <param name="clientId">The client's id who will get the extrainfo.</param>
        /// <param name="extraInfo">The desired extrainfo's data.</param>
        /// <returns>Redirects us to GetTrainer.</returns>
        [HttpPost]
        public IActionResult CreateInfo(string clientId, ExtraInfo extraInfo)
        {
            if (extraInfo != null)
            {
                extraInfo.InfoId = Guid.NewGuid().ToString();
                this.clientLogic.AddInfoToClient(extraInfo, clientId);
                return this.RedirectToAction(nameof(this.GetTrainer), new { this.clientLogic.GetClient(clientId).TrainerID });
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Sends a Get request to create an Trainer.
        /// </summary>
        /// <returns>Redirects us to CreateTrainer.</returns>
        [HttpGet]
        public IActionResult CreateTrainer()
        {
            return this.View();
        }

        /// <summary>
        /// Creates the new trainer and adds it to the trainer repo.
        /// </summary>
        /// <param name="trainer">The desired trainer's data.</param>
        /// <returns>Redirects us to ListTrainer.</returns>
        [HttpPost]
        public IActionResult CreateTrainer(Trainer trainer)
        {
            if (trainer != null)
            {
                trainer.TrainerID = Guid.NewGuid().ToString();
                this.trainerLogic.AddTrainer(trainer);
                return this.RedirectToAction(nameof(this.ListTrainer));
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Allows us to look at a specific trainer's clients.
        /// </summary>
        /// <param name="trainerId">The trainer's id.</param>
        /// <returns>Redirects us to GetTrainer.</returns>
        public IActionResult GetTrainer(string trainerId)
        {
            return this.View(this.trainerLogic.GetTrainer(trainerId));
        }

        /// <summary>
        /// Lists all trainers.
        /// </summary>
        /// <returns>Redirects us to ListTrainer.</returns>
        public IActionResult ListTrainer()
        {
            return this.View(this.trainerLogic.GetTrainers());
        }

        /// <summary>
        /// Sends a get request to update a client.
        /// </summary>
        /// <param name="clientId">The client's id.</param>
        /// <returns>Redirects us to UpdateClient.</returns>
        [HttpGet]
        public IActionResult UpdateClient(string clientId)
        {
            return this.View(this.clientLogic.GetClient(clientId));
        }

        /// <summary>
        /// Updates the data of a client.
        /// </summary>
        /// <param name="newClient">The client's desired data.</param>
        /// <returns>Redirects us to GetTrainer.</returns>
        [HttpPost]
        public IActionResult UpdateClient(GymClient newClient)
        {
            if (newClient != null)
            {
                this.clientLogic.UpdateClient(newClient.GymID, newClient);
                return this.RedirectToAction(nameof(this.GetTrainer), new { newClient.TrainerID });
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Sends a get request to update an extrainfo.
        /// </summary>
        /// <param name="infoId">The extrainfo's id.</param>
        /// <returns>Redirects us to GetInfo.</returns>
        [HttpGet]
        public IActionResult UpdateInfo(string infoId)
        {
            return this.View(this.infoLogic.GetInfo(infoId));
        }

        /// <summary>
        /// Updates the specific extrainfo.
        /// </summary>
        /// <param name="newInfo">The desired extrainfo's data.</param>
        /// <returns>Redirects us to GetTrainer.</returns>
        [HttpPost]
        public IActionResult UpdateInfo(ExtraInfo newInfo)
        {
            if (newInfo != null)
            {
                this.infoLogic.UpdateInfo(newInfo.InfoId, newInfo);
                return this.RedirectToAction(nameof(this.GetTrainer), new
                {
                    this.clientLogic.GetClient(newInfo.GymID).TrainerID,
                });
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Sends a get request to update a trainer.
        /// </summary>
        /// <param name="trainerId">The trainer's id.</param>
        /// <returns>Redirects us to UpdateTrainer.</returns>
        [HttpGet]
        public IActionResult UpdateTrainer(string trainerId)
        {
            return this.View(this.trainerLogic.GetTrainer(trainerId));
        }

        /// <summary>
        /// Updates a trainer.
        /// </summary>
        /// <param name="newTrainer">The desired trainer's data.</param>
        /// <returns>Redirects us to GetTrainer.</returns>
        [HttpPost]
        public IActionResult UpdateTrainer(Trainer newTrainer)
        {
            if (newTrainer != null)
            {
                this.trainerLogic.UpdateTrainer(newTrainer.TrainerID, newTrainer);
                return this.RedirectToAction(nameof(this.GetTrainer), new
                {
                    newTrainer.TrainerID,
                });
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Deletes a client and all their extrainfo, also they get removed from their trainer.
        /// </summary>
        /// <param name="clientId">The client's id who will be deleted.</param>
        /// <returns>Redirects us to GetTrainer.</returns>
        public IActionResult DeleteClient(string clientId)
        {
            var infoToDelete = this.clientLogic.GetClient(clientId).ExtraInfos.ToArray();
            for (int i = 0; i < infoToDelete.Length; i++)
            {
                this.clientLogic.RemoveInfoFromClient(infoToDelete[i], clientId);
                this.infoLogic.DeleteInfo(infoToDelete[i].InfoId);
            }

            var clientToDelete = this.clientLogic.GetClient(clientId);
            string trainerId = clientToDelete.TrainerID;
            this.trainerLogic.RemoveClientFromTrainer(clientToDelete, trainerId);
            this.clientLogic.DeleteClient(clientId);
            return this.RedirectToAction(nameof(this.GetTrainer), new { trainerId });
        }

        /// <summary>
        /// Deletes an extrainfo.
        /// </summary>
        /// <param name="infoId">The extrainfo's id that will be deleted.</param>
        /// <returns>Redirects us to GetTrainer.</returns>
        public IActionResult DeleteInfo(string infoId)
        {
            var infoToDelete = this.infoLogic.GetInfo(infoId);
            string clientId = infoToDelete.GymClient.GymID;
            this.clientLogic.RemoveInfoFromClient(infoToDelete, clientId);
            this.infoLogic.DeleteInfo(infoId);
            return this.RedirectToAction(nameof(this.GetTrainer), new { infoToDelete.GymClient.TrainerID });
        }

        /// <summary>
        /// Deletes a trainer and all their clients alongside all their extrainfos.
        /// </summary>
        /// <param name="trainerId">The trainer's id who will be deleted.</param>
        /// <returns>Redirects us to ListTrainer.</returns>
        public IActionResult DeleteTrainer(string trainerId)
        {
            var clientsToDelete = this.trainerLogic.GetTrainer(trainerId).GymClients.ToArray();
            for (int i = 0; i < clientsToDelete.Length; i++)
            {
                this.trainerLogic.RemoveClientFromTrainer(clientsToDelete[i], trainerId);
                foreach (var item in clientsToDelete[i].ExtraInfos)
                {
                    this.clientLogic.RemoveInfoFromClient(item, clientsToDelete[i].GymID);
                }

                this.clientLogic.DeleteClient(clientsToDelete[i].GymID);
            }

            this.trainerLogic.DeleteTrainer(trainerId);
            return this.RedirectToAction(nameof(this.ListTrainer));
        }
    }
}