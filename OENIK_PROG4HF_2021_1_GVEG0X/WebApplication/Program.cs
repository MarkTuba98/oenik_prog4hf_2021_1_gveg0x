//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
[assembly: System.CLSCompliant(false)]

namespace WebApplication
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;

    /// <summary>
    /// A class for our main program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// A method where start and configure a builder object.
        /// </summary>
        /// <param name="args">The arguments?.</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Generated code, it creates a host.
        /// </summary>
        /// <param name="args">The required arguments.</param>
        /// <returns>An IHostBuilder that will be built and configured.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
