﻿//-----------------------------------------------------------------------
// <copyright file="GymContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
[assembly: System.CLSCompliant(false)]

namespace Data
{
    using Microsoft.EntityFrameworkCore;
    using Models;

    /// <summary>
    /// A class for our database context.
    /// Containts all database sets, constructors and methods for creating the database such as OnConfiguring and OnModelCreating.
    /// </summary>
    public class GymContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GymContext"/> class.
        /// </summary>
        /// <param name="opt">The options to be used by our database context.</param>
        public GymContext(DbContextOptions<GymContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GymContext"/> class.
        /// </summary>
        public GymContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets the clients.
        /// </summary>
        public DbSet<GymClient> GymClients { get; set; }

        /// <summary>
        /// Gets or sets the trainers.
        /// </summary>
        public DbSet<Trainer> Trainers { get; set; }

        /// <summary>
        /// Gets or sets the extra information about our clients.
        /// </summary>
        public DbSet<ExtraInfo> ExtraInfos { get; set; }

        /// <summary>
        /// A method where we configure the database to be used for this context.
        /// </summary>
        /// <param name="optionsBuilder">The options to be used by our database context.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies().UseSqlServer(@"data source=(LocalDB)\MSSQLLocalDB;attachdbfilename=|DataDirectory|\GymDB.mdf;integrated security=True;MultipleActiveResultSets=True");
            }
        }

        /// <summary>
        /// A method where we configure the model when it is created.
        /// </summary>
        /// <param name="modelBuilder">The builder being used to construct the model for this context.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder != null)
            {
                base.OnModelCreating(modelBuilder);
                modelBuilder.Entity<GymClient>(entity =>
                {
                    entity.HasOne(client => client.Trainer).WithMany(trainer => trainer.GymClients).HasForeignKey(client => client.TrainerID);
                });
                modelBuilder.Entity<ExtraInfo>(entity =>
                {
                    entity.HasOne(infos => infos.GymClient).WithMany(client => client.ExtraInfos).HasForeignKey(infos => infos.GymID);
                });
            }
        }
    }
}