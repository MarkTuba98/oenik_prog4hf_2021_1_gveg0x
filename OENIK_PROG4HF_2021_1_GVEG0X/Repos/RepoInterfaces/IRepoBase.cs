﻿//-----------------------------------------------------------------------
// <copyright file="IRepoBase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Repos
{
    using System.Linq;

    /// <summary>
    /// An interface that serves as a base for all our repositories.
    /// </summary>
    /// <typeparam name="T">It takes any kind of "type" of objects, configured by the repositories.</typeparam>
    public interface IRepoBase<T>
        where T : new()
    {
        /// <summary>
        /// A method that adds an item to the repository.
        /// </summary>
        /// <param name="item">The item about to be added to the repo.</param>
        void Add(T item);

        /// <summary>
        /// A method that deletes an item from the repository.
        /// </summary>
        /// <param name="item">The item about to be deleted from the repo.</param>
        void Delete(T item);

        /// <summary>
        /// A method that deletes an item from the repository.
        /// </summary>
        /// <param name="id">Based on the id an item from the repo will be deleted.</param>
        void Delete(string id);

        /// <summary>
        /// A method that returns all items from the repository.
        /// </summary>
        /// <returns>All items from the repository.</returns>
        IQueryable<T> Read();

        /// <summary>
        /// A method that updates an item in the repository.
        /// </summary>
        /// <param name="gymID">The id of the item that needs changing.</param>
        /// <param name="newItem">The object containing all information that is gonna be updated accordingly.</param>
        void Update(string gymID, T newItem);

        /// <summary>
        /// A method that returns an item from the repository.
        /// </summary>
        /// <param name="gymID">The id of the item you want to get.</param>
        /// <returns>Returns a T object.</returns>
        T GetItem(string gymID);

        /// <summary>
        /// A method that saves the changes made to the repository.
        /// </summary>
        void Save();
    }
}
