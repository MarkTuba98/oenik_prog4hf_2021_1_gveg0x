﻿//-----------------------------------------------------------------------
// <copyright file="InfoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Repos
{
    using System.Linq;
    using Data;
    using Models;

    /// <summary>
    /// A class for the information repository, that inherits from our base repository interface.
    /// Contains various CRUD methods for the information repository.
    /// </summary>
    public class InfoRepository : IRepoBase<Models.ExtraInfo>
    {
        private readonly GymContext context = new ();

        /// <summary>
        /// A method for adding new information to the repository.
        /// </summary>
        /// <param name="item">The information about to be added to the repository.</param>
        public void Add(ExtraInfo item)
        {
            this.context.ExtraInfos.Add(item);
            this.Save();
        }

        /// <summary>
        /// A method for deleting information from the repository.
        /// </summary>
        /// <param name="item">The information about to be deleted from the repository.</param>
        public void Delete(ExtraInfo item)
        {
            this.context.ExtraInfos.Remove(item);
            this.Save();
        }

        /// <summary>
        /// A method for deleting information from the repository.
        /// </summary>
        /// <param name="id">The id of the information you wish to delete.</param>
        public void Delete(string id)
        {
            this.Delete(this.GetItem(id));
        }

        /// <summary>
        /// A method that returns a single information's object based on the id given.
        /// </summary>
        /// <param name="gymID">The information's id that you want to get.</param>
        /// <returns> The desired ExtraInfo object.</returns>
        public ExtraInfo GetItem(string gymID)
        {
            return this.context.ExtraInfos.FirstOrDefault(t => t.InfoId == gymID);
        }

        /// <summary>
        /// A method that returns all of the "extra information" as an IQueryable.
        /// </summary>
        /// <returns> All extra information in the repository.</returns>
        public IQueryable<ExtraInfo> Read()
        {
            return this.context.ExtraInfos.AsQueryable();
        }

        /// <summary>
        /// A method that saves all changes done to the repository.
        /// </summary>
        public void Save()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// A method that updates a client.
        /// </summary>
        /// <param name="gymID"> The id of the information you wish to update.</param>
        /// <param name="newItem">An object containing all the information that need to be updated.</param>
        public void Update(string gymID, ExtraInfo newItem)
        {
            if (newItem != null)
            {
                var oldItem = this.GetItem(gymID);
                oldItem.Information = newItem.Information;
                this.Save();
            }
        }
    }
}
