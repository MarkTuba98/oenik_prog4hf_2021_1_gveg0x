﻿//-----------------------------------------------------------------------
// <copyright file="TrainerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Repos
{
    using System.Linq;
    using Data;
    using Models;

    /// <summary>
    /// A class for the trainer repository, that inherits from our base repository interface.
    /// Contains various CRUD methods for the trainer repository.
    /// </summary>
    public class TrainerRepository : IRepoBase<Models.Trainer>
    {
        private readonly GymContext context = new ();

        /// <summary>
        /// A method for adding new trainer to the repository.
        /// </summary>
        /// <param name="item">The trainer about to be added to the repository.</param>
        public void Add(Trainer item)
        {
            this.context.Trainers.Add(item);
            this.Save();
        }

        /// <summary>
        /// A method for trainer information from the repository.
        /// </summary>
        /// <param name="item">The trainer about to be deleted from the repository.</param>
        public void Delete(Trainer item)
        {
            this.context.Trainers.Remove(item);
            this.Save();
        }

        /// <summary>
        /// A method for deleting trainer from the repository.
        /// </summary>
        /// <param name="id">The id of the trainer you wish to delete.</param>
        public void Delete(string id)
        {
            this.Delete(this.GetItem(id));
        }

        /// <summary>
        /// A method that returns a single trainer's object based on the id given.
        /// </summary>
        /// <param name="gymID">The trainer's id that you want to get.</param>
        /// <returns> The desired Trainer object.</returns>
        public Trainer GetItem(string gymID)
        {
            return this.context.Trainers.FirstOrDefault(t => t.TrainerID == gymID);
        }

        /// <summary>
        /// A method that returns all of the trainers as an IQueryable.
        /// </summary>
        /// <returns> All trainers in the repository.</returns>
        public IQueryable<Trainer> Read()
        {
            return this.context.Trainers.AsQueryable();
        }

        /// <summary>
        /// A method that saves all changes done to the repository.
        /// </summary>
        public void Save()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// A method that updates a trainer.
        /// </summary>
        /// <param name="gymID"> The id of the trainer you wish to update.</param>
        /// <param name="newItem">An object containing all the information that need to be updated.</param>
        public void Update(string gymID, Trainer newItem)
        {
            if (newItem != null)
            {
                var oldItem = this.GetItem(gymID);
                oldItem.TrainerName = newItem.TrainerName;
                oldItem.Selected = newItem.Selected;
                this.Save();
            }
        }
    }
}
