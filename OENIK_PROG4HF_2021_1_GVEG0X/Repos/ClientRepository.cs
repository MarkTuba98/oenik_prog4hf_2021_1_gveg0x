﻿//-----------------------------------------------------------------------
// <copyright file="ClientRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
[assembly: System.CLSCompliant(false)]

namespace Repos
{
    using System.Linq;
    using Data;
    using Models;

    /// <summary>
    /// A class for the client repository, that inherits from our base repository interface.
    /// Contains various CRUD methods for the client repository.
    /// </summary>
    public class ClientRepository : IRepoBase<GymClient>
    {
        private readonly GymContext context = new ();

        /// <summary>
        /// A method for adding new clients to the repository.
        /// </summary>
        /// <param name="item">The client about to be added to the repository.</param>
        public void Add(GymClient item)
        {
            this.context.GymClients.Add(item);
            this.Save();
        }

        /// <summary>
        /// A method for deleting clients from the repository.
        /// </summary>
        /// <param name="item">The client about to be deleted from the repository.</param>
        public void Delete(GymClient item)
        {
            this.context.GymClients.Remove(item);
            this.Save();
        }

        /// <summary>
        /// A method for deleting a single client from the repository.
        /// </summary>
        /// <param name="id">The id of the client you wish to delete.</param>
        public void Delete(string id)
        {
            this.Delete(this.GetItem(id));
        }

        /// <summary>
        /// A method that returns a single client's object based on the id given.
        /// </summary>
        /// <param name="gymID">The client's id that you want to get.</param>
        /// <returns> The desired GymClient object.</returns>
        public GymClient GetItem(string gymID)
        {
            return this.context.GymClients.FirstOrDefault(g => (g.GymID == gymID));
        }

        /// <summary>
        /// A method that returns all of the clients as an IQueryable.
        /// </summary>
        /// <returns> All clients in the repository.</returns>
        public System.Linq.IQueryable<GymClient> Read()
        {
            return this.context.GymClients.AsQueryable();
        }

        /// <summary>
        /// A method that saves all changes done to the repository.
        /// </summary>
        public void Save()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// A method that updates a client.
        /// </summary>
        /// <param name="gymID"> The id of the client that needs to be updated.</param>
        /// <param name="newItem"> An object containing all the details that need to be updated.</param>
        public void Update(string gymID, GymClient newItem)
        {
            if (newItem != null)
            {
                var oldItem = this.GetItem(gymID);
                oldItem.FullName = newItem.FullName;
                oldItem.Age = newItem.Age;
                oldItem.Gender = newItem.Gender;
                oldItem.BeenWorkingOutFor = newItem.BeenWorkingOutFor;
                oldItem.Verified = newItem.Verified;
                oldItem.TrainerID = newItem.TrainerID;
                this.Save();
            }
        }
    }
}
