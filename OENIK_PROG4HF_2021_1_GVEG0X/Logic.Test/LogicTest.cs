﻿//-----------------------------------------------------------------------
// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
[assembly: System.CLSCompliant(false)]

namespace Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using Moq;
    using NUnit.Framework;
    using Repos;

    /// <summary>
    /// A class made for testing the logic layer of our program.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IRepoBase<GymClient>> clientRepository;
        private Mock<IRepoBase<ExtraInfo>> infoRepository;
        private Mock<IRepoBase<Trainer>> trainerRepository;

        /// <summary>
        /// A method where we initialize most of the things about our mock repositories before we get into testing.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.clientRepository = new Mock<IRepoBase<GymClient>>();
            this.infoRepository = new Mock<IRepoBase<ExtraInfo>>();
            this.trainerRepository = new Mock<IRepoBase<Trainer>>();

            List<GymClient> clients = new ();
            List<ExtraInfo> infos = new ();
            List<Trainer> trainers = new ();

            GymClient g1 = new ()
            {
                GymID = "test00",
                Gender = Genders.Nő,
                FullName = "Hennyes ZereGacskó",
                TrainerID = "asd647",
                Verified = false,
                Age = 27,
                BeenWorkingOutFor = 0,
            };
            GymClient g2 = new ()
            {
                GymID = "test01",
                Gender = Genders.Férfi,
                FullName = "Komodoros Zacsinyalogato",
                TrainerID = "klo293",
                Verified = false,
                Age = 16,
                BeenWorkingOutFor = 2,
            };
            GymClient g3 = new ()
            {
                GymID = "test02",
                Gender = Genders.Férfi,
                FullName = "Arnold Zacsinegger",
                TrainerID = "huge99",
                Verified = true,
                Age = 33,
                BeenWorkingOutFor = 11,
            };
            GymClient g4 = new ()
            {
                GymID = "test03",
                Gender = Genders.Nő,
                FullName = "Pesti veszettmacska",
                TrainerID = "asd647",
                Verified = true,
                Age = 24,
                BeenWorkingOutFor = 5,
            };
            GymClient g5 = new ()
            {
                GymID = "test04",
                Gender = Genders.Férfi,
                FullName = "obudai kronikuskigyo",
                TrainerID = "asd647",
                Verified = false,
                Age = 12,
                BeenWorkingOutFor = 0,
            };

            ExtraInfo i1 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Nem szeret lábazni" };
            ExtraInfo i2 = new () { InfoId = Guid.NewGuid().ToString(), Information = "váll problémái vannak" };
            ExtraInfo i3 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Nem szeret élni" };
            ExtraInfo i4 = new () { InfoId = Guid.NewGuid().ToString(), Information = "Depressziós" };

            Trainer t1 = new () { TrainerName = "Edvás Erezacsi", TrainerID = Guid.NewGuid().ToString() };
            Trainer t2 = new () { TrainerName = "Zacsi Maszíro", TrainerID = "trainer02" };
            Trainer t3 = new () { TrainerName = "Medvés Hasnyálmokus", TrainerID = Guid.NewGuid().ToString() };

            clients.Add(g1);
            clients.Add(g2);
            clients.Add(g3);
            clients.Add(g4);
            clients.Add(g5);

            infos.Add(i1);
            infos.Add(i2);
            infos.Add(i3);
            infos.Add(i4);

            trainers.Add(t1);
            trainers.Add(t2);
            trainers.Add(t3);

            this.clientRepository.Setup(x => x.Read()).Returns(clients.AsQueryable());
            this.infoRepository.Setup(x => x.Read()).Returns(infos.AsQueryable());
            this.trainerRepository.Setup(x => x.Read()).Returns(trainers.AsQueryable());

            this.clientRepository.Setup(x => x.GetItem(It.IsAny<string>())).Returns(clients[1]);
            this.trainerRepository.Setup(x => x.GetItem(It.IsAny<string>())).Returns(trainers[2]);
        }

        /// <summary>
        /// A test case where we test whether the Add method is working or not.
        /// </summary>
        [Test]
        public void TrainerAddMethod()
        {
            TrainerLogic trainerLogic = new (this.trainerRepository.Object);

            Trainer trainer = new () { TrainerID = "sasfalkon", TrainerName = "Ron melléfoly" };
            trainerLogic.AddTrainer(trainer);

            this.trainerRepository.Verify(x => x.Add(trainer), Times.Once);
        }

        /// <summary>
        /// A test case where we test whether the Read method is working or not.
        /// </summary>
        [Test]
        public void ClientReadMethodComparingToStringMethods()
        {
            ClientLogic clientLogic = new (this.clientRepository.Object);

            var gymClient = clientLogic.GetClient("test01");
            GymClient proposedClient = new ()
            {
                GymID = "test01",
                Gender = Genders.Férfi,
                FullName = "Komodoros Zacsinyalogato",
                TrainerID = "klo293",
                Verified = false,
                Age = 16,
                BeenWorkingOutFor = 2,
            };

            Assert.That(gymClient.ToString(), Is.EqualTo(proposedClient.ToString()));

            this.clientRepository.Verify(x => x.Read(), Times.Never);
            this.clientRepository.Verify(x => x.GetItem(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// A test case where we test whether the Update method is working or not.
        /// </summary>
        [Test]
        public void UpdateTrainerMethodTesting()
        {
            TrainerLogic trainerLogic = new (this.trainerRepository.Object);

            Trainer wantedTrainer = new () { TrainerName = "Zacsi Maszíro" };
            trainerLogic.UpdateTrainer("trainer02", wantedTrainer);
            this.trainerRepository.Verify(x => x.Update(It.IsAny<string>(), It.IsAny<Trainer>()), Times.Once);
        }

        /// <summary>
        /// A test case where we test whether the Delete method is working or not.
        /// </summary>
        [Test]
        public void DeleteClientFromLogic()
        {
            ClientLogic clientLogic = new (this.clientRepository.Object);
            Assert.That(clientLogic.GetClients().Count, Is.EqualTo(5));

            clientLogic.DeleteClient("test01");

            this.clientRepository.Verify(x => x.Delete(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// A test case where we test whether the Delete method is working or not.
        /// </summary>
        [Test]
        public void DeleteInfoFromLogic()
        {
            ExtraInfoLogic extraInfoLogic = new (this.infoRepository.Object);
            Assert.That(extraInfoLogic.GetInfos().Count, Is.EqualTo(4));

            extraInfoLogic.DeleteInfo("i4");

            this.infoRepository.Verify(x => x.Delete(It.IsAny<string>()), Times.Once);
        }
    }
}
