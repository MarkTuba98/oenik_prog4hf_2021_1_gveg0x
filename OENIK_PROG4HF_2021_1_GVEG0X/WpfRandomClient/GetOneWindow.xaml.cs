﻿//-----------------------------------------------------------------------
// <copyright file="GetOneWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfRandomClient
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using WpfRandomClient.VM;

    /// <summary>
    /// Interaction logic for GetOneWindow.xaml.
    /// </summary>
    public partial class GetOneWindow : Window
    {
        private MainViewModel VM;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetOneWindow"/> class.
        /// </summary>
        public GetOneWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GetOneWindow"/> class.
        /// </summary>
        /// <param name="helper">The amount of times GetOne will be called.</param>
        public GetOneWindow(int helper)
            : this()
        {
            this.VM = new MainViewModel(helper, new MainLogic());
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.VM;
            this.VM.MainTimer.Start();

            Messenger.Default.Register<string>(this, "TrainerResult", msg =>
            {
                MessageBox.Show(msg);
            });
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            this.VM.MainTimer.Stop();
            this.VM.DeleteAll();
            Messenger.Default.Unregister(this);
        }
    }
}
