﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfRandomClient
{
    using WpfRandomClient.VM;

    /// <summary>
    /// Interface for main logic.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// SendMessage.
        /// </summary>
        /// <param name="success">Bool.</param>
        public void SendMessage(bool success);

        /// <summary>
        /// Api Get One method.
        /// </summary>
        /// <returns>A trainerVM.</returns>
        public TrainerVM ApiGetOne();

        /// <summary>
        /// Api delete a trainer.
        /// </summary>
        /// <param name="model">Trainer view model.</param>
        public void ApiDelTrainer(TrainerVM model);

        /// <summary>
        /// Api Select a random trainer.
        /// </summary>
        /// <param name="model">Requires a model.</param>
        public void ApiSelectRand(TrainerVM model);

        /// <summary>
        /// Api Unselect a random trainer.
        /// </summary>
        /// <param name="model">Requires a model.</param>
        public void ApiUnselectRand(TrainerVM model);
    }
}
