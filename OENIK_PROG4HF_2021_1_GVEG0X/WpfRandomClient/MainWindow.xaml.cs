﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfRandomClient
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// If the click is ok do this.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">Event arguments.</param>
        private void OkClick(object sender, RoutedEventArgs eventArgs)
        {
            if (!string.IsNullOrEmpty(this.Textbox.Text))
            {
                int helper = int.Parse(this.Textbox.Text);
                if (helper < 0 || helper > 50)
                {
                    helper = 3;
                }

                GetOneWindow getOne = new (helper);
                getOne.Show();
            }
        }
    }
}
