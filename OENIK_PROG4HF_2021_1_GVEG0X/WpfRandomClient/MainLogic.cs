﻿//-----------------------------------------------------------------------
// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfRandomClient
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text.Json;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Messaging;
    using WpfRandomClient.VM;

    /// <summary>
    /// Main logic.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "http://localhost:5000/Random/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <inheritdoc/>
        public void SendMessage(bool success)
        {
            string msg = success ? "success" : "failed";
            Messenger.Default.Send(msg, "TrainerResult");
        }

        /// <inheritdoc/>
        public TrainerVM ApiGetOne()
        {
            string json = this.client.GetStringAsync(this.url + "GetOne").Result;
            var helper = JsonSerializer.Deserialize<TrainerVM>(json, this.jsonOptions);

            return helper;
        }

        /// <inheritdoc/>
        public void ApiDelTrainer(TrainerVM model)
        {
            bool success = false;
            if (model != null)
            {
                string newURL = "http://localhost:5000/TrainerApi/del/" + model.TrainerID;

                string json = this.client.GetStringAsync(newURL).Result;
                JsonDocument doc = JsonDocument.Parse(json);

                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }
        }

        /// <inheritdoc/>
        public void ApiSelectRand(TrainerVM model)
        {
            if (model != null)
            {
                string helper = this.url + "Select/" + model.TrainerID;
                string json = this.client.GetStringAsync(helper).Result;
                JsonDocument doc = JsonDocument.Parse(json);
            }
        }

        /// <inheritdoc/>
        public void ApiUnselectRand(TrainerVM model)
        {
            if (model != null)
            {
                string helper = this.url + "Unselect/" + model.TrainerID;
                string json = this.client.GetStringAsync(helper).Result;
                JsonDocument doc = JsonDocument.Parse(json);
            }
        }
    }
}
