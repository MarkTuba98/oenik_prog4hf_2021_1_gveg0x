﻿// <copyright file="CommandVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfRandomClient.VM
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// A View Model for commands.
    /// </summary>
    public class CommandVM : ObservableObject
    {
        private string commandRan;

        /// <summary>
        /// Gets or sets the command ran.
        /// </summary>
        public string CommandRan
        {
            get { return this.commandRan; }
            set { this.Set(ref this.commandRan, value); }
        }
    }
}
