﻿//-----------------------------------------------------------------------
// <copyright file="TrainerVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfRandomClient.VM
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Trainer view model.
    /// </summary>
    public class TrainerVM : ObservableObject
    {
        private string trainerID;
        private string trainerName;
        private bool selected;

        /// <summary>
        /// Gets or sets the trainer's id.
        /// </summary>
        public string TrainerID
        {
            get { return this.trainerID; }
            set { this.Set(ref this.trainerID, value); }
        }

        /// <summary>
        /// Gets or sets the trainer's name.
        /// </summary>
        public string TrainerName
        {
            get { return this.trainerName; }
            set { this.Set(ref this.trainerName, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the trainer's selected.
        /// </summary>
        public bool Selected
        {
            get { return this.selected; }
            set { this.Set(ref this.selected, value); }
        }

        /// <summary>
        /// Copy from other vm.
        /// </summary>
        /// <param name="other">TrainerVm.</param>
        public void CopyFrom(TrainerVM other)
        {
            if (other is null)
            {
                return;
            }

            this.TrainerID = other.TrainerID;
            this.TrainerName = other.TrainerName;
            this.Selected = other.Selected;
        }
    }
}
