﻿//-----------------------------------------------------------------------
// <copyright file="DarabszamVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfRandomClient.VM
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Darabszam view model.
    /// </summary>
    public class DarabszamVM : ObservableObject
    {
        private int darabszam;

        /// <summary>
        /// Gets or sets the darabszam.
        /// </summary>
        public int Darabszam
        {
            get { return this.darabszam; }
            set { this.Set(ref this.darabszam, value); }
        }
    }
}
