﻿//-----------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfRandomClient.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Threading;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Class.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Main timer for selection change.
        /// </summary>
        public DispatcherTimer MainTimer;

        private IMainLogic logic;
        private ObservableCollection<TrainerVM> allTrainers;
        private ObservableCollection<CommandVM> commands;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="helper">The amount of times GetOne will be called.</param>
        /// <param name="logic">Logic.</param>
        public MainViewModel(int helper, IMainLogic logic)
        {
            this.MainTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(2),
            };
            this.MainTimer.Tick += new EventHandler(this.dispatcherTimer_Tick);
            this.Commands = new ();
            this.logic = logic;
            this.Trainers = new ObservableCollection<TrainerVM>();

            for (int i = 0; i < helper; i++)
            {
                var trainObj = this.logic.ApiGetOne();

                if (trainObj != null)
                {
                    this.Trainers.Add(trainObj);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
           : this(0, IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets all trainers.
        /// </summary>
        public ObservableCollection<TrainerVM> Trainers
        {
            get { return this.allTrainers; }
            set { this.Set(ref this.allTrainers, value); }
        }

        /// <summary>
        /// Gets or sets the commands to display.
        /// </summary>
        public ObservableCollection<CommandVM> Commands
        {
            get { return this.commands; }
            set { this.Set(ref this.commands, value); }
        }

        /// <summary>
        /// Deletes all Trainers.
        /// </summary>
        public void DeleteAll()
        {
            foreach (var item in this.Trainers)
            {
                this.logic.ApiDelTrainer(item);
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            Random r = new Random();
            string commandPerfomed = "The following trainer named, ";
            int helper = r.Next(this.Trainers.Count);
            commandPerfomed += this.Trainers[helper].TrainerName + " will be ";

            if (this.Trainers[helper].Selected)
            {
                this.logic.ApiUnselectRand(this.Trainers[helper]);
                this.Trainers[helper].Selected = false;
                commandPerfomed += "unselected.";
            }
            else
            {
                this.logic.ApiSelectRand(this.Trainers[helper]);
                this.Trainers[helper].Selected = true;
                commandPerfomed += "selected.";
            }

            this.Commands.Add(new CommandVM { CommandRan = commandPerfomed });
        }
    }
}
