﻿//-----------------------------------------------------------------------
// <copyright file="MyIoc.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace WpfRandomClient
{
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// A class that implements both an Ioc and a service locator.
    /// </summary>
    public class MyIoc : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets an instance of the class.
        /// </summary>
        public static MyIoc Instance { get; private set; } = new MyIoc();
    }
}