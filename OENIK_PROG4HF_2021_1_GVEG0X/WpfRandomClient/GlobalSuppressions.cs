﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Reviewed>", Scope = "type", Target = "~T:WpfRandomClient.MainLogic")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfRandomClient.MainLogic.SendMessage(System.Boolean)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfRandomClient.MainWindow.OkClick(System.Object,System.Windows.RoutedEventArgs)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfRandomClient.MainLogic.ApiDelTrainer(WpfRandomClient.VM.TrainerVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfRandomClient.MainLogic.ApiGetOne~WpfRandomClient.VM.TrainerVM")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfRandomClient.MainLogic.ApiSelectRand(WpfRandomClient.VM.TrainerVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfRandomClient.MainLogic.ApiUnselectRand(WpfRandomClient.VM.TrainerVM)")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:Field names should begin with lower-case letter", Justification = "<Reviewed>", Scope = "member", Target = "~F:WpfRandomClient.GetOneWindow.VM")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfRandomClient.VM.MainViewModel.dispatcherTimer_Tick(System.Object,System.EventArgs)")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "<Reviewed>", Scope = "member", Target = "~M:WpfRandomClient.VM.MainViewModel.dispatcherTimer_Tick(System.Object,System.EventArgs)")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Reviewed>", Scope = "member", Target = "~F:WpfRandomClient.VM.MainViewModel.MainTimer")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<Reviewed>", Scope = "member", Target = "~F:WpfRandomClient.VM.MainViewModel.MainTimer")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Reviewed>", Scope = "member", Target = "~P:WpfRandomClient.VM.MainViewModel.Trainers")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Reviewed>", Scope = "member", Target = "~P:WpfRandomClient.VM.MainViewModel.Commands")]
