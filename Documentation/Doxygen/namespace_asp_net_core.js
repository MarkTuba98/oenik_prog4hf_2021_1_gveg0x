var namespace_asp_net_core =
[
    [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
    [ "Views_Home_CreateClient", "class_asp_net_core_1_1_views___home___create_client.html", "class_asp_net_core_1_1_views___home___create_client" ],
    [ "Views_Home_CreateInfo", "class_asp_net_core_1_1_views___home___create_info.html", "class_asp_net_core_1_1_views___home___create_info" ],
    [ "Views_Home_CreateTrainer", "class_asp_net_core_1_1_views___home___create_trainer.html", "class_asp_net_core_1_1_views___home___create_trainer" ],
    [ "Views_Home_GetTrainer", "class_asp_net_core_1_1_views___home___get_trainer.html", "class_asp_net_core_1_1_views___home___get_trainer" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_ListTrainer", "class_asp_net_core_1_1_views___home___list_trainer.html", "class_asp_net_core_1_1_views___home___list_trainer" ],
    [ "Views_Home_Statistics", "class_asp_net_core_1_1_views___home___statistics.html", "class_asp_net_core_1_1_views___home___statistics" ],
    [ "Views_Home_UpdateClient", "class_asp_net_core_1_1_views___home___update_client.html", "class_asp_net_core_1_1_views___home___update_client" ],
    [ "Views_Home_UpdateInfo", "class_asp_net_core_1_1_views___home___update_info.html", "class_asp_net_core_1_1_views___home___update_info" ],
    [ "Views_Home_UpdateTrainer", "class_asp_net_core_1_1_views___home___update_trainer.html", "class_asp_net_core_1_1_views___home___update_trainer" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ]
];