var class_logic_1_1_client_logic =
[
    [ "ClientLogic", "class_logic_1_1_client_logic.html#a4c2a391e91d7555db96e4fcff9915ab2", null ],
    [ "AddClient", "class_logic_1_1_client_logic.html#ae2c11d8da4ec2b7084c15799a3851a32", null ],
    [ "AddInfoToClient", "class_logic_1_1_client_logic.html#afaed04e065982a5adfefed347e5c33cd", null ],
    [ "AmountOfAlcoholists", "class_logic_1_1_client_logic.html#ac7cf070e8c84a972a6ad94ee6cee107a", null ],
    [ "DeleteClient", "class_logic_1_1_client_logic.html#a0c4b98183369ad8496fc6d7a8f236fc5", null ],
    [ "FillDbWithSamples", "class_logic_1_1_client_logic.html#a58702a64fcc00e8bdc341fccf6aee2f1", null ],
    [ "GetClient", "class_logic_1_1_client_logic.html#a1d44307e93c6330e7b76f41de6cab5ec", null ],
    [ "GetClients", "class_logic_1_1_client_logic.html#a7496caacd553ec21edb443276dbd9bc2", null ],
    [ "LongestInfo", "class_logic_1_1_client_logic.html#a564badb987359594583a59490522fade", null ],
    [ "RemoveInfoFromClient", "class_logic_1_1_client_logic.html#a6ab7240f64f4c16faf8ea393f4838542", null ],
    [ "UpdateClient", "class_logic_1_1_client_logic.html#a7ecb02ddb3e3124145e139c8b25eb946", null ]
];