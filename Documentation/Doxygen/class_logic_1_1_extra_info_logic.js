var class_logic_1_1_extra_info_logic =
[
    [ "ExtraInfoLogic", "class_logic_1_1_extra_info_logic.html#a02741ea2fbc6873d575eb567a995202d", null ],
    [ "AddInfo", "class_logic_1_1_extra_info_logic.html#a1c5d44337203ba344aee885bf4484bb1", null ],
    [ "DeleteInfo", "class_logic_1_1_extra_info_logic.html#a61e3a833f8d9edc24804c5c4df950730", null ],
    [ "GetInfo", "class_logic_1_1_extra_info_logic.html#ad50abcb66b6422c71d6092de8482a0b1", null ],
    [ "GetInfos", "class_logic_1_1_extra_info_logic.html#a8c456ca370f1bb3dcd1396dafc062433", null ],
    [ "UpdateInfo", "class_logic_1_1_extra_info_logic.html#abcb986abda8b851efd61df81db1f7b7f", null ]
];