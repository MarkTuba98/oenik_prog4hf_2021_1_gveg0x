var interface_repos_1_1_i_repo_base =
[
    [ "Add", "interface_repos_1_1_i_repo_base.html#ae747735573f9a3ab42fc7b356096dd4d", null ],
    [ "Delete", "interface_repos_1_1_i_repo_base.html#a8c75fdfc25c60435f73b9716ea59d46e", null ],
    [ "Delete", "interface_repos_1_1_i_repo_base.html#a8e96bb203cb90a4d6e85f887cd5c77cc", null ],
    [ "GetItem", "interface_repos_1_1_i_repo_base.html#af949984f15b81433c7ee96bf6ae1dc20", null ],
    [ "Read", "interface_repos_1_1_i_repo_base.html#af520cfc242feb7f5c374d0db5320f70a", null ],
    [ "Save", "interface_repos_1_1_i_repo_base.html#ae372c98724ce5c281c6b99f89fe5d8be", null ],
    [ "Update", "interface_repos_1_1_i_repo_base.html#a5a1f433ffedfa2c34b99a19b4bfd4dec", null ]
];