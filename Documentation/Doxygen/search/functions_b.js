var searchData=
[
  ['read_273',['Read',['../class_repos_1_1_client_repository.html#a1c7fd49cc38c6cb0cc385d06ff3b1773',1,'Repos.ClientRepository.Read()'],['../class_repos_1_1_info_repository.html#aab3b7331241b78edf4b36bafcf50d674',1,'Repos.InfoRepository.Read()'],['../interface_repos_1_1_i_repo_base.html#af520cfc242feb7f5c374d0db5320f70a',1,'Repos.IRepoBase.Read()'],['../class_repos_1_1_trainer_repository.html#acf1ae43fe6d60096ac003277bb65e4ff',1,'Repos.TrainerRepository.Read()']]],
  ['removeclientfromtrainer_274',['RemoveClientFromTrainer',['../class_logic_1_1_trainer_logic.html#a088c1fe51c976504cc0e36a7ad6d51df',1,'Logic::TrainerLogic']]],
  ['removeinfofromclient_275',['RemoveInfoFromClient',['../class_logic_1_1_client_logic.html#a6ab7240f64f4c16faf8ea393f4838542',1,'Logic::ClientLogic']]]
];
