var searchData=
[
  ['gender_50',['Gender',['../class_models_1_1_gym_client.html#aad30d83e1ac59499819e57d385b15d1c',1,'Models::GymClient']]],
  ['genderpercentage_51',['GenderPercentage',['../class_logic_1_1_trainer_logic.html#ada42a0a337bfa5718f32269a58539db8',1,'Logic::TrainerLogic']]],
  ['genders_52',['Genders',['../namespace_models.html#a977e1d3eda1b67d326f823ed765640b0',1,'Models']]],
  ['generatedinternaltypehelper_53',['GeneratedInternalTypeHelper',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html',1,'XamlGeneratedNamespace']]],
  ['getalltrainers_54',['GetAllTrainers',['../interface_wpf_app_1_1_b_l_1_1_i_u_i_trainer_logic.html#a6e776b6a2302f88b35a3f019b7e02f80',1,'WpfApp.BL.IUITrainerLogic.GetAllTrainers()'],['../class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html#aab5c7ccee211b4e24d6125eb3079f6ba',1,'WpfApp.BL.UITrainerLogic.GetAllTrainers()']]],
  ['getclient_55',['GetClient',['../class_logic_1_1_client_logic.html#a1d44307e93c6330e7b76f41de6cab5ec',1,'Logic::ClientLogic']]],
  ['getclients_56',['GetClients',['../class_logic_1_1_client_logic.html#a7496caacd553ec21edb443276dbd9bc2',1,'Logic::ClientLogic']]],
  ['getgenderpercentage_57',['GetGenderPercentage',['../class_models_1_1_statistics.html#a08888177bfc8dc7a5d61158c8db4070c',1,'Models::Statistics']]],
  ['getinfo_58',['GetInfo',['../class_logic_1_1_extra_info_logic.html#ad50abcb66b6422c71d6092de8482a0b1',1,'Logic::ExtraInfoLogic']]],
  ['getinfos_59',['GetInfos',['../class_logic_1_1_extra_info_logic.html#a8c456ca370f1bb3dcd1396dafc062433',1,'Logic::ExtraInfoLogic']]],
  ['getitem_60',['GetItem',['../class_repos_1_1_client_repository.html#a6f42850e9a9093c8b33210138412783a',1,'Repos.ClientRepository.GetItem()'],['../class_repos_1_1_info_repository.html#a04dca33d03d5823a22b9e1bf4defeb76',1,'Repos.InfoRepository.GetItem()'],['../interface_repos_1_1_i_repo_base.html#af949984f15b81433c7ee96bf6ae1dc20',1,'Repos.IRepoBase.GetItem()'],['../class_repos_1_1_trainer_repository.html#a3a7506991c0a7fcb7f3b3b66399b2fae',1,'Repos.TrainerRepository.GetItem()']]],
  ['getpropertyvalue_61',['GetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)']]],
  ['gettrainer_62',['GetTrainer',['../class_logic_1_1_trainer_logic.html#ab52a989b064a9a48e56c9a96ecc1b5ca',1,'Logic.TrainerLogic.GetTrainer()'],['../class_web_application_1_1_controllers_1_1_home_controller.html#a1da5b84740ee8481339a54983a89f26a',1,'WebApplication.Controllers.HomeController.GetTrainer()']]],
  ['gettrainers_63',['GetTrainers',['../class_logic_1_1_trainer_logic.html#a37feeedb11457f34092ade7fe4a6250b',1,'Logic::TrainerLogic']]],
  ['gymclient_64',['GymClient',['../class_models_1_1_extra_info.html#ac745100d4cf06ac5dbfe9ed6f327fdc0',1,'Models.ExtraInfo.GymClient()'],['../class_models_1_1_gym_client.html',1,'Models.GymClient']]],
  ['gymclients_65',['GymClients',['../class_data_1_1_gym_context.html#ae5f181332f46458285a6bef053ba48ba',1,'Data.GymContext.GymClients()'],['../class_models_1_1_trainer.html#aae88e279633e470f5d9abf992d21f8ef',1,'Models.Trainer.GymClients()']]],
  ['gymcontext_66',['GymContext',['../class_data_1_1_gym_context.html',1,'Data.GymContext'],['../class_data_1_1_gym_context.html#a3c6e2a3b60894d704ea709a48d4f3b1d',1,'Data.GymContext.GymContext(DbContextOptions&lt; GymContext &gt; opt)'],['../class_data_1_1_gym_context.html#a79bb12b1f696e6e1028e56993d1d70a6',1,'Data.GymContext.GymContext()']]],
  ['gymid_67',['GymID',['../class_models_1_1_extra_info.html#a4affc9e33721af977c1cf909e620c786',1,'Models.ExtraInfo.GymID()'],['../class_models_1_1_gym_client.html#aa8b7608f1861722b1bae609c0db10125',1,'Models.GymClient.GymID()']]]
];
