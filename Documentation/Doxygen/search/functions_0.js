var searchData=
[
  ['add_214',['Add',['../class_repos_1_1_client_repository.html#aa71c5bf0b7103334e69513ee982b3e92',1,'Repos.ClientRepository.Add()'],['../class_repos_1_1_info_repository.html#ad951a11536e1af69ee7089081aa663c5',1,'Repos.InfoRepository.Add()'],['../interface_repos_1_1_i_repo_base.html#ae747735573f9a3ab42fc7b356096dd4d',1,'Repos.IRepoBase.Add()'],['../class_repos_1_1_trainer_repository.html#a801b4109f2452c7e680f49b3346588fe',1,'Repos.TrainerRepository.Add()']]],
  ['addclient_215',['AddClient',['../class_logic_1_1_client_logic.html#ae2c11d8da4ec2b7084c15799a3851a32',1,'Logic::ClientLogic']]],
  ['addclienttotrainer_216',['AddClientToTrainer',['../class_logic_1_1_trainer_logic.html#ae776b08a56be5011f9f724efdb0ddb8c',1,'Logic::TrainerLogic']]],
  ['addeventhandler_217',['AddEventHandler',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a73471f4a6d1ca4c4fceec9ad8610f0c8',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.AddEventHandler(System.Reflection.EventInfo eventInfo, object target, System.Delegate handler)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a73471f4a6d1ca4c4fceec9ad8610f0c8',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.AddEventHandler(System.Reflection.EventInfo eventInfo, object target, System.Delegate handler)']]],
  ['addinfo_218',['AddInfo',['../class_logic_1_1_extra_info_logic.html#a1c5d44337203ba344aee885bf4484bb1',1,'Logic::ExtraInfoLogic']]],
  ['addinfotoclient_219',['AddInfoToClient',['../class_logic_1_1_client_logic.html#afaed04e065982a5adfefed347e5c33cd',1,'Logic::ClientLogic']]],
  ['addtrainer_220',['AddTrainer',['../class_logic_1_1_trainer_logic.html#a24bd0851dae26410d4f2241ff8fb719d',1,'Logic.TrainerLogic.AddTrainer()'],['../interface_wpf_app_1_1_b_l_1_1_i_u_i_trainer_logic.html#a4fba2d9b7c61d4a193eb46670b1e2113',1,'WpfApp.BL.IUITrainerLogic.AddTrainer()'],['../class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html#a79455b5945292868a0afcdbecd497b59',1,'WpfApp.BL.UITrainerLogic.AddTrainer()']]],
  ['amountofalcoholists_221',['AmountOfAlcoholists',['../class_logic_1_1_client_logic.html#ac7cf070e8c84a972a6ad94ee6cee107a',1,'Logic::ClientLogic']]],
  ['amountofclients_222',['AmountOfClients',['../class_logic_1_1_trainer_logic.html#aebb893aa8c23759035e08d1b5fa59a5c',1,'Logic::TrainerLogic']]],
  ['amountofextrainfo_223',['AmountOfExtraInfo',['../class_logic_1_1_trainer_logic.html#a7bce1dc1b0cc7d65d7c4bb27180fd15e',1,'Logic::TrainerLogic']]],
  ['amountoftrainers_224',['AmountOfTrainers',['../class_logic_1_1_trainer_logic.html#a954524086d5473e7a9af19780a74e83c',1,'Logic::TrainerLogic']]],
  ['app_225',['App',['../class_wpf_app_1_1_app.html#aba5855b304f331b77fee3a336c175b55',1,'WpfApp::App']]],
  ['averageage_226',['AverageAge',['../class_logic_1_1_trainer_logic.html#a777bcd0618bbed9e220105d1c9a3c040',1,'Logic::TrainerLogic']]]
];
