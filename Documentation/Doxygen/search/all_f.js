var searchData=
[
  ['save_106',['Save',['../class_repos_1_1_client_repository.html#ad66374a058f3894d18325542b6cd1e3b',1,'Repos.ClientRepository.Save()'],['../class_repos_1_1_info_repository.html#adfed78824674d29486cfe6c501896bdc',1,'Repos.InfoRepository.Save()'],['../interface_repos_1_1_i_repo_base.html#ae372c98724ce5c281c6b99f89fe5d8be',1,'Repos.IRepoBase.Save()'],['../class_repos_1_1_trainer_repository.html#aad09e727d25b21c6e41fa3303aeb71fc',1,'Repos.TrainerRepository.Save()']]],
  ['selectedtrainer_107',['SelectedTrainer',['../class_wpf_app_1_1_v_m_1_1_main_view_model.html#adc384c233e06464e136a6e6c109a8d8a',1,'WpfApp::VM::MainViewModel']]],
  ['setgenderpercentage_108',['SetGenderPercentage',['../class_models_1_1_statistics.html#ab67c8d09a462ed67718aa62515c3baeb',1,'Models::Statistics']]],
  ['setpropertyvalue_109',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)']]],
  ['setup_110',['Setup',['../class_logic_1_1_test_1_1_logic_test.html#af6b8fa14c148fe2d97954ee11ab8940d',1,'Logic::Test::LogicTest']]],
  ['startup_111',['Startup',['../class_web_application_1_1_startup.html',1,'WebApplication']]],
  ['statistics_112',['Statistics',['../class_models_1_1_statistics.html',1,'Models.Statistics'],['../class_web_application_1_1_controllers_1_1_home_controller.html#a77a1212ad0eb6b9ea9bc8249643222f5',1,'WebApplication.Controllers.HomeController.Statistics()']]],
  ['statustobrushconverter_113',['StatusToBrushConverter',['../class_wpf_app_1_1_user_interface_1_1_status_to_brush_converter.html',1,'WpfApp::UserInterface']]]
];
