var searchData=
[
  ['add_0',['Add',['../class_repos_1_1_client_repository.html#aa71c5bf0b7103334e69513ee982b3e92',1,'Repos.ClientRepository.Add()'],['../class_repos_1_1_info_repository.html#ad951a11536e1af69ee7089081aa663c5',1,'Repos.InfoRepository.Add()'],['../interface_repos_1_1_i_repo_base.html#ae747735573f9a3ab42fc7b356096dd4d',1,'Repos.IRepoBase.Add()'],['../class_repos_1_1_trainer_repository.html#a801b4109f2452c7e680f49b3346588fe',1,'Repos.TrainerRepository.Add()']]],
  ['addclient_1',['AddClient',['../class_logic_1_1_client_logic.html#ae2c11d8da4ec2b7084c15799a3851a32',1,'Logic::ClientLogic']]],
  ['addclienttotrainer_2',['AddClientToTrainer',['../class_logic_1_1_trainer_logic.html#ae776b08a56be5011f9f724efdb0ddb8c',1,'Logic::TrainerLogic']]],
  ['addcmd_3',['AddCmd',['../class_wpf_app_1_1_v_m_1_1_main_view_model.html#a97ceea343d7cc0ef0da1d1e0d7b78991',1,'WpfApp::VM::MainViewModel']]],
  ['addeventhandler_4',['AddEventHandler',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a73471f4a6d1ca4c4fceec9ad8610f0c8',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.AddEventHandler(System.Reflection.EventInfo eventInfo, object target, System.Delegate handler)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a73471f4a6d1ca4c4fceec9ad8610f0c8',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.AddEventHandler(System.Reflection.EventInfo eventInfo, object target, System.Delegate handler)']]],
  ['addinfo_5',['AddInfo',['../class_logic_1_1_extra_info_logic.html#a1c5d44337203ba344aee885bf4484bb1',1,'Logic::ExtraInfoLogic']]],
  ['addinfotoclient_6',['AddInfoToClient',['../class_logic_1_1_client_logic.html#afaed04e065982a5adfefed347e5c33cd',1,'Logic::ClientLogic']]],
  ['addtrainer_7',['AddTrainer',['../class_logic_1_1_trainer_logic.html#a24bd0851dae26410d4f2241ff8fb719d',1,'Logic.TrainerLogic.AddTrainer()'],['../interface_wpf_app_1_1_b_l_1_1_i_u_i_trainer_logic.html#a4fba2d9b7c61d4a193eb46670b1e2113',1,'WpfApp.BL.IUITrainerLogic.AddTrainer()'],['../class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html#a79455b5945292868a0afcdbecd497b59',1,'WpfApp.BL.UITrainerLogic.AddTrainer()']]],
  ['age_8',['Age',['../class_models_1_1_gym_client.html#a9ab4d5a79107667a455170470ed2330e',1,'Models::GymClient']]],
  ['amountofalcoholists_9',['AmountOfAlcoholists',['../class_models_1_1_statistics.html#a7c7b3580556a08b8daba51d936230abf',1,'Models.Statistics.AmountOfAlcoholists()'],['../class_logic_1_1_client_logic.html#ac7cf070e8c84a972a6ad94ee6cee107a',1,'Logic.ClientLogic.AmountOfAlcoholists()']]],
  ['amountofclients_10',['AmountOfClients',['../class_models_1_1_statistics.html#af53408fc2dba15f32a8c1db09cdafe72',1,'Models.Statistics.AmountOfClients()'],['../class_logic_1_1_trainer_logic.html#aebb893aa8c23759035e08d1b5fa59a5c',1,'Logic.TrainerLogic.AmountOfClients()']]],
  ['amountofextrainfo_11',['AmountOfExtraInfo',['../class_models_1_1_statistics.html#a7767c56308fe7c4a26040d1e6d96e447',1,'Models.Statistics.AmountOfExtraInfo()'],['../class_logic_1_1_trainer_logic.html#a7bce1dc1b0cc7d65d7c4bb27180fd15e',1,'Logic.TrainerLogic.AmountOfExtraInfo()']]],
  ['amountoftrainers_12',['AmountOfTrainers',['../class_models_1_1_statistics.html#aff31522bd8c1bdc22e332cdcc38aa2e0',1,'Models.Statistics.AmountOfTrainers()'],['../class_logic_1_1_trainer_logic.html#a954524086d5473e7a9af19780a74e83c',1,'Logic.TrainerLogic.AmountOfTrainers()']]],
  ['app_13',['App',['../class_wpf_app_1_1_app.html#aba5855b304f331b77fee3a336c175b55',1,'WpfApp.App.App()'],['../class_wpf_app_1_1_app.html',1,'WpfApp.App']]],
  ['aspnetcore_14',['AspNetCore',['../namespace_asp_net_core.html',1,'']]],
  ['averageage_15',['AverageAge',['../class_logic_1_1_trainer_logic.html#a777bcd0618bbed9e220105d1c9a3c040',1,'Logic::TrainerLogic']]],
  ['averageagevalue_16',['AverageAgeValue',['../class_models_1_1_statistics.html#a1834bdea9b9cc56671d86e8e0c12c823',1,'Models::Statistics']]]
];
