var searchData=
[
  ['editorserviceviawindow_40',['EditorServiceViaWindow',['../class_wpf_app_1_1_u_i_1_1_editor_service_via_window.html',1,'WpfApp::UI']]],
  ['editorviewmodel_41',['EditorViewModel',['../class_wpf_app_1_1_v_m_1_1_editor_view_model.html',1,'WpfApp::VM']]],
  ['editorwindow_42',['EditorWindow',['../class_wpf_app_1_1_u_i_1_1_editor_window.html#af13ee1dd527437608e0209ae67ff3db9',1,'WpfApp.UI.EditorWindow.EditorWindow()'],['../class_wpf_app_1_1_u_i_1_1_editor_window.html#a808adcb60908c014a6d3ad160b77d6c2',1,'WpfApp.UI.EditorWindow.EditorWindow(Trainer oldTrainer)'],['../class_wpf_app_1_1_u_i_1_1_editor_window.html',1,'WpfApp.UI.EditorWindow'],['../class_wpf_app_1_1_user_interface_1_1_editor_window.html',1,'WpfApp.UserInterface.EditorWindow']]],
  ['edittrainer_43',['EditTrainer',['../interface_wpf_app_1_1_b_l_1_1_i_editor_service.html#a57b2df68ae7201a3bbf51352cef27f29',1,'WpfApp.BL.IEditorService.EditTrainer()'],['../class_wpf_app_1_1_u_i_1_1_editor_service_via_window.html#a0e1d660551c610a4fadeebb37cc1059c',1,'WpfApp.UI.EditorServiceViaWindow.EditTrainer()']]],
  ['extrainfo_44',['ExtraInfo',['../class_models_1_1_extra_info.html',1,'Models']]],
  ['extrainfologic_45',['ExtraInfoLogic',['../class_logic_1_1_extra_info_logic.html#a02741ea2fbc6873d575eb567a995202d',1,'Logic.ExtraInfoLogic.ExtraInfoLogic()'],['../class_logic_1_1_extra_info_logic.html',1,'Logic.ExtraInfoLogic']]],
  ['extrainfos_46',['ExtraInfos',['../class_data_1_1_gym_context.html#a41c84c13a316b3993967d7e5f40608bf',1,'Data.GymContext.ExtraInfos()'],['../class_models_1_1_gym_client.html#a31eb87a2c91dd89af84d997118d13cb1',1,'Models.GymClient.ExtraInfos()']]]
];
