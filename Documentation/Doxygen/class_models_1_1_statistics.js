var class_models_1_1_statistics =
[
    [ "GetGenderPercentage", "class_models_1_1_statistics.html#a08888177bfc8dc7a5d61158c8db4070c", null ],
    [ "SetGenderPercentage", "class_models_1_1_statistics.html#ab67c8d09a462ed67718aa62515c3baeb", null ],
    [ "AmountOfAlcoholists", "class_models_1_1_statistics.html#a7c7b3580556a08b8daba51d936230abf", null ],
    [ "AmountOfClients", "class_models_1_1_statistics.html#af53408fc2dba15f32a8c1db09cdafe72", null ],
    [ "AmountOfExtraInfo", "class_models_1_1_statistics.html#a7767c56308fe7c4a26040d1e6d96e447", null ],
    [ "AmountOfTrainers", "class_models_1_1_statistics.html#aff31522bd8c1bdc22e332cdcc38aa2e0", null ],
    [ "AverageAgeValue", "class_models_1_1_statistics.html#a1834bdea9b9cc56671d86e8e0c12c823", null ],
    [ "LongestInfo", "class_models_1_1_statistics.html#ae3b8492272c500a1b9e86ab0f85458e5", null ]
];