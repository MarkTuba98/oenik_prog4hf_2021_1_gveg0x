var namespace_models =
[
    [ "ExtraInfo", "class_models_1_1_extra_info.html", "class_models_1_1_extra_info" ],
    [ "GymClient", "class_models_1_1_gym_client.html", "class_models_1_1_gym_client" ],
    [ "Statistics", "class_models_1_1_statistics.html", "class_models_1_1_statistics" ],
    [ "Trainer", "class_models_1_1_trainer.html", "class_models_1_1_trainer" ],
    [ "Genders", "namespace_models.html#a977e1d3eda1b67d326f823ed765640b0", [
      [ "Nő", "namespace_models.html#a977e1d3eda1b67d326f823ed765640b0a9802ebcf3feb3d2def49f6255301bdd6", null ],
      [ "Férfi", "namespace_models.html#a977e1d3eda1b67d326f823ed765640b0a017cd52da7df8315e3d7433f8fca369f", null ],
      [ "Helikopter", "namespace_models.html#a977e1d3eda1b67d326f823ed765640b0a624d8e6eb27881661fa58d8a1df9156c", null ]
    ] ]
];