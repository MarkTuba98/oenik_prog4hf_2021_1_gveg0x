var class_web_application_1_1_controllers_1_1_home_controller =
[
    [ "HomeController", "class_web_application_1_1_controllers_1_1_home_controller.html#adc346a14f3e2852680d1c87b7d5b72b3", null ],
    [ "CreateClient", "class_web_application_1_1_controllers_1_1_home_controller.html#a66ca5bf17255fab0d00f99aad094ca37", null ],
    [ "CreateClient", "class_web_application_1_1_controllers_1_1_home_controller.html#aacc354d18d98ea74bde6dc9ac61a65bc", null ],
    [ "CreateInfo", "class_web_application_1_1_controllers_1_1_home_controller.html#ab7954b4b328ed6cea5e64964a3807221", null ],
    [ "CreateInfo", "class_web_application_1_1_controllers_1_1_home_controller.html#af17d7364833d4f6c9cf4eb6edc0733bd", null ],
    [ "CreateTrainer", "class_web_application_1_1_controllers_1_1_home_controller.html#ad1b5374a76f0dc8b9e1ac5d239831f13", null ],
    [ "CreateTrainer", "class_web_application_1_1_controllers_1_1_home_controller.html#a3d93224e680beb375aea5c9541adfedf", null ],
    [ "DeleteClient", "class_web_application_1_1_controllers_1_1_home_controller.html#a5108d77e0cdfa56d8806450cbd414551", null ],
    [ "DeleteInfo", "class_web_application_1_1_controllers_1_1_home_controller.html#a7e2289feee13ec58f67ff8eb7b197a27", null ],
    [ "DeleteTrainer", "class_web_application_1_1_controllers_1_1_home_controller.html#aa865c070b8d0e2e110e18c2be31bb0dd", null ],
    [ "GetTrainer", "class_web_application_1_1_controllers_1_1_home_controller.html#a1da5b84740ee8481339a54983a89f26a", null ],
    [ "Index", "class_web_application_1_1_controllers_1_1_home_controller.html#a12368a9aa97809f8e9f4680954a14017", null ],
    [ "Init", "class_web_application_1_1_controllers_1_1_home_controller.html#a0771caa0622ca2fd5f31dd6fc2cf6912", null ],
    [ "ListTrainer", "class_web_application_1_1_controllers_1_1_home_controller.html#aac17ee07dfb086208db66ed0d8238ca0", null ],
    [ "Statistics", "class_web_application_1_1_controllers_1_1_home_controller.html#a77a1212ad0eb6b9ea9bc8249643222f5", null ],
    [ "UpdateClient", "class_web_application_1_1_controllers_1_1_home_controller.html#ad6a23767ed5ab8f9d14bbaa6b1990f89", null ],
    [ "UpdateClient", "class_web_application_1_1_controllers_1_1_home_controller.html#a47e04deb2455b80ece2377a6abe0cdc1", null ],
    [ "UpdateInfo", "class_web_application_1_1_controllers_1_1_home_controller.html#a74b152796b802d531a5265092a50b033", null ],
    [ "UpdateInfo", "class_web_application_1_1_controllers_1_1_home_controller.html#ac192813c2b0e650a5c06f37893c9c4bb", null ],
    [ "UpdateTrainer", "class_web_application_1_1_controllers_1_1_home_controller.html#a979cccbc6eb6bf9f6851e861d3196e0b", null ],
    [ "UpdateTrainer", "class_web_application_1_1_controllers_1_1_home_controller.html#a70909bc6f1a0c8074876542e112ff72e", null ]
];