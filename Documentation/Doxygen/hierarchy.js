var hierarchy =
[
    [ "Application", null, [
      [ "WpfApp.App", "class_wpf_app_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "WpfApp.App", "class_wpf_app_1_1_app.html", null ],
      [ "WpfApp.App", "class_wpf_app_1_1_app.html", null ]
    ] ],
    [ "Logic.ClientLogic", "class_logic_1_1_client_logic.html", null ],
    [ "Controller", null, [
      [ "WebApplication.Controllers.HomeController", "class_web_application_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "Data.GymContext", "class_data_1_1_gym_context.html", null ]
    ] ],
    [ "Models.ExtraInfo", "class_models_1_1_extra_info.html", null ],
    [ "Logic.ExtraInfoLogic", "class_logic_1_1_extra_info_logic.html", null ],
    [ "Models.GymClient", "class_models_1_1_gym_client.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "WpfApp.UserInterface.EditorWindow", "class_wpf_app_1_1_user_interface_1_1_editor_window.html", null ],
      [ "WpfApp.UserInterface.StatusToBrushConverter", "class_wpf_app_1_1_user_interface_1_1_status_to_brush_converter.html", null ]
    ] ],
    [ "WpfApp.BL.IEditorService", "interface_wpf_app_1_1_b_l_1_1_i_editor_service.html", [
      [ "WpfApp.UI.EditorServiceViaWindow", "class_wpf_app_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Repos.IRepoBase< T >", "interface_repos_1_1_i_repo_base.html", null ],
    [ "Repos.IRepoBase< GymClient >", "interface_repos_1_1_i_repo_base.html", [
      [ "Repos.ClientRepository", "class_repos_1_1_client_repository.html", null ]
    ] ],
    [ "Repos.IRepoBase< Models.ExtraInfo >", "interface_repos_1_1_i_repo_base.html", [
      [ "Repos.InfoRepository", "class_repos_1_1_info_repository.html", null ]
    ] ],
    [ "Repos.IRepoBase< Models.Trainer >", "interface_repos_1_1_i_repo_base.html", [
      [ "Repos.TrainerRepository", "class_repos_1_1_trainer_repository.html", null ]
    ] ],
    [ "Repos.IRepoBase< Models.ExtraInfo >", "interface_repos_1_1_i_repo_base.html", null ],
    [ "Repos.IRepoBase< Models.GymClient >", "interface_repos_1_1_i_repo_base.html", null ],
    [ "Repos.IRepoBase< Models.Trainer >", "interface_repos_1_1_i_repo_base.html", null ],
    [ "IServiceLocator", null, [
      [ "WpfApp.MyIoc", "class_wpf_app_1_1_my_ioc.html", null ]
    ] ],
    [ "WpfApp.BL.IUITrainerLogic", "interface_wpf_app_1_1_b_l_1_1_i_u_i_trainer_logic.html", [
      [ "WpfApp.BL.UITrainerLogic", "class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html", null ]
    ] ],
    [ "Logic.Test.LogicTest", "class_logic_1_1_test_1_1_logic_test.html", null ],
    [ "ConsoleApp.Program", "class_console_app_1_1_program.html", null ],
    [ "WebApplication.Program", "class_web_application_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_CreateClient", "class_asp_net_core_1_1_views___home___create_client.html", null ],
      [ "AspNetCore.Views_Home_CreateClient", "class_asp_net_core_1_1_views___home___create_client.html", null ],
      [ "AspNetCore.Views_Home_CreateInfo", "class_asp_net_core_1_1_views___home___create_info.html", null ],
      [ "AspNetCore.Views_Home_CreateInfo", "class_asp_net_core_1_1_views___home___create_info.html", null ],
      [ "AspNetCore.Views_Home_CreateTrainer", "class_asp_net_core_1_1_views___home___create_trainer.html", null ],
      [ "AspNetCore.Views_Home_CreateTrainer", "class_asp_net_core_1_1_views___home___create_trainer.html", null ],
      [ "AspNetCore.Views_Home_GetTrainer", "class_asp_net_core_1_1_views___home___get_trainer.html", null ],
      [ "AspNetCore.Views_Home_GetTrainer", "class_asp_net_core_1_1_views___home___get_trainer.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Statistics", "class_asp_net_core_1_1_views___home___statistics.html", null ],
      [ "AspNetCore.Views_Home_Statistics", "class_asp_net_core_1_1_views___home___statistics.html", null ],
      [ "AspNetCore.Views_Home_UpdateClient", "class_asp_net_core_1_1_views___home___update_client.html", null ],
      [ "AspNetCore.Views_Home_UpdateClient", "class_asp_net_core_1_1_views___home___update_client.html", null ],
      [ "AspNetCore.Views_Home_UpdateInfo", "class_asp_net_core_1_1_views___home___update_info.html", null ],
      [ "AspNetCore.Views_Home_UpdateInfo", "class_asp_net_core_1_1_views___home___update_info.html", null ],
      [ "AspNetCore.Views_Home_UpdateTrainer", "class_asp_net_core_1_1_views___home___update_trainer.html", null ],
      [ "AspNetCore.Views_Home_UpdateTrainer", "class_asp_net_core_1_1_views___home___update_trainer.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IQueryable< Models.Trainer >>", null, [
      [ "AspNetCore.Views_Home_ListTrainer", "class_asp_net_core_1_1_views___home___list_trainer.html", null ],
      [ "AspNetCore.Views_Home_ListTrainer", "class_asp_net_core_1_1_views___home___list_trainer.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "WpfApp.MyIoc", "class_wpf_app_1_1_my_ioc.html", null ]
    ] ],
    [ "WebApplication.Startup", "class_web_application_1_1_startup.html", null ],
    [ "Models.Statistics", "class_models_1_1_statistics.html", null ],
    [ "WpfApp.DataGeneration.TestDataGeneration", "class_wpf_app_1_1_data_generation_1_1_test_data_generation.html", null ],
    [ "Models.Trainer", "class_models_1_1_trainer.html", null ],
    [ "Logic.TrainerLogic", "class_logic_1_1_trainer_logic.html", null ],
    [ "ViewModelBase", null, [
      [ "WpfApp.VM.EditorViewModel", "class_wpf_app_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "WpfApp.VM.MainViewModel", "class_wpf_app_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "WpfApp.UI.EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "WpfApp.UserInterface.EditorWindow", "class_wpf_app_1_1_user_interface_1_1_editor_window.html", null ],
      [ "WpfApp.UserInterface.StatusToBrushConverter", "class_wpf_app_1_1_user_interface_1_1_status_to_brush_converter.html", null ]
    ] ],
    [ "Window", null, [
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ]
    ] ]
];