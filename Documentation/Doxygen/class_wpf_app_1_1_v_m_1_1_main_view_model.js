var class_wpf_app_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_wpf_app_1_1_v_m_1_1_main_view_model.html#a0d86cb53942a315fa0500d7c382f8a96", null ],
    [ "MainViewModel", "class_wpf_app_1_1_v_m_1_1_main_view_model.html#a901c7bea4fd9f2401d7429f63cb80342", null ],
    [ "AddCmd", "class_wpf_app_1_1_v_m_1_1_main_view_model.html#a97ceea343d7cc0ef0da1d1e0d7b78991", null ],
    [ "DelCmd", "class_wpf_app_1_1_v_m_1_1_main_view_model.html#a32ac933f381dd6ae69f1775182364bbb", null ],
    [ "ModCmd", "class_wpf_app_1_1_v_m_1_1_main_view_model.html#aa518e27c9ba8a9104e98d5ab1968ed72", null ],
    [ "SelectedTrainer", "class_wpf_app_1_1_v_m_1_1_main_view_model.html#adc384c233e06464e136a6e6c109a8d8a", null ],
    [ "Trainers", "class_wpf_app_1_1_v_m_1_1_main_view_model.html#a54e6ce23c5f45771c82ae8773dbf6347", null ]
];