var namespace_repos =
[
    [ "ClientRepository", "class_repos_1_1_client_repository.html", "class_repos_1_1_client_repository" ],
    [ "InfoRepository", "class_repos_1_1_info_repository.html", "class_repos_1_1_info_repository" ],
    [ "IRepoBase", "interface_repos_1_1_i_repo_base.html", "interface_repos_1_1_i_repo_base" ],
    [ "TrainerRepository", "class_repos_1_1_trainer_repository.html", "class_repos_1_1_trainer_repository" ]
];