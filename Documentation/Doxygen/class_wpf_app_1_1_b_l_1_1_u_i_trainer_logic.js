var class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic =
[
    [ "UITrainerLogic", "class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html#af035d0f6d63c1b25948daa7b534eedc5", null ],
    [ "AddTrainer", "class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html#a79455b5945292868a0afcdbecd497b59", null ],
    [ "DelTrainer", "class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html#ada78e7bcba8745feab426f61440bc24c", null ],
    [ "GetAllTrainers", "class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html#aab5c7ccee211b4e24d6125eb3079f6ba", null ],
    [ "ModTrainer", "class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html#ae3261d17b342b06b83a9ce10e99969fe", null ]
];