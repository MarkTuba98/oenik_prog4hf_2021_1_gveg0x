var class_models_1_1_gym_client =
[
    [ "ToString", "class_models_1_1_gym_client.html#a3c6efeb73a8b36589605c6547faa0171", null ],
    [ "Age", "class_models_1_1_gym_client.html#a9ab4d5a79107667a455170470ed2330e", null ],
    [ "BeenWorkingOutFor", "class_models_1_1_gym_client.html#ad7b45980d55703739209e189c39f8a69", null ],
    [ "ExtraInfos", "class_models_1_1_gym_client.html#a31eb87a2c91dd89af84d997118d13cb1", null ],
    [ "FullName", "class_models_1_1_gym_client.html#a404f7942cbaa2494cfdce9a229921051", null ],
    [ "Gender", "class_models_1_1_gym_client.html#aad30d83e1ac59499819e57d385b15d1c", null ],
    [ "GymID", "class_models_1_1_gym_client.html#aa8b7608f1861722b1bae609c0db10125", null ],
    [ "Trainer", "class_models_1_1_gym_client.html#a6bb7f09271dd4c42def3383ee166ce7e", null ],
    [ "TrainerID", "class_models_1_1_gym_client.html#a5f9bf8e974ec0604e93d1992b0406736", null ],
    [ "Verified", "class_models_1_1_gym_client.html#a4000a285dc92de42ad29088c08f3d307", null ]
];