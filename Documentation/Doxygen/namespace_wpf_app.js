var namespace_wpf_app =
[
    [ "BL", "namespace_wpf_app_1_1_b_l.html", "namespace_wpf_app_1_1_b_l" ],
    [ "DataGeneration", "namespace_wpf_app_1_1_data_generation.html", "namespace_wpf_app_1_1_data_generation" ],
    [ "UI", "namespace_wpf_app_1_1_u_i.html", "namespace_wpf_app_1_1_u_i" ],
    [ "UserInterface", "namespace_wpf_app_1_1_user_interface.html", "namespace_wpf_app_1_1_user_interface" ],
    [ "VM", "namespace_wpf_app_1_1_v_m.html", "namespace_wpf_app_1_1_v_m" ],
    [ "App", "class_wpf_app_1_1_app.html", "class_wpf_app_1_1_app" ],
    [ "MainWindow", "class_wpf_app_1_1_main_window.html", "class_wpf_app_1_1_main_window" ],
    [ "MyIoc", "class_wpf_app_1_1_my_ioc.html", "class_wpf_app_1_1_my_ioc" ]
];