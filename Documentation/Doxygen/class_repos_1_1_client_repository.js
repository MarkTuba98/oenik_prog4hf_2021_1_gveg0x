var class_repos_1_1_client_repository =
[
    [ "Add", "class_repos_1_1_client_repository.html#aa71c5bf0b7103334e69513ee982b3e92", null ],
    [ "Delete", "class_repos_1_1_client_repository.html#a940fe2067c40aae418ad8cc4c65cdc63", null ],
    [ "Delete", "class_repos_1_1_client_repository.html#a25baeefa59dc9e606456be09dca52576", null ],
    [ "GetItem", "class_repos_1_1_client_repository.html#a6f42850e9a9093c8b33210138412783a", null ],
    [ "Read", "class_repos_1_1_client_repository.html#a1c7fd49cc38c6cb0cc385d06ff3b1773", null ],
    [ "Save", "class_repos_1_1_client_repository.html#ad66374a058f3894d18325542b6cd1e3b", null ],
    [ "Update", "class_repos_1_1_client_repository.html#a1910d97d44e81ee385568b6864bbe4d4", null ]
];