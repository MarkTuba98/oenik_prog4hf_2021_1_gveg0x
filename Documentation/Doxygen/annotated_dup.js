var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Home_CreateClient", "class_asp_net_core_1_1_views___home___create_client.html", "class_asp_net_core_1_1_views___home___create_client" ],
      [ "Views_Home_CreateInfo", "class_asp_net_core_1_1_views___home___create_info.html", "class_asp_net_core_1_1_views___home___create_info" ],
      [ "Views_Home_CreateTrainer", "class_asp_net_core_1_1_views___home___create_trainer.html", "class_asp_net_core_1_1_views___home___create_trainer" ],
      [ "Views_Home_GetTrainer", "class_asp_net_core_1_1_views___home___get_trainer.html", "class_asp_net_core_1_1_views___home___get_trainer" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_ListTrainer", "class_asp_net_core_1_1_views___home___list_trainer.html", "class_asp_net_core_1_1_views___home___list_trainer" ],
      [ "Views_Home_Statistics", "class_asp_net_core_1_1_views___home___statistics.html", "class_asp_net_core_1_1_views___home___statistics" ],
      [ "Views_Home_UpdateClient", "class_asp_net_core_1_1_views___home___update_client.html", "class_asp_net_core_1_1_views___home___update_client" ],
      [ "Views_Home_UpdateInfo", "class_asp_net_core_1_1_views___home___update_info.html", "class_asp_net_core_1_1_views___home___update_info" ],
      [ "Views_Home_UpdateTrainer", "class_asp_net_core_1_1_views___home___update_trainer.html", "class_asp_net_core_1_1_views___home___update_trainer" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ]
    ] ],
    [ "ConsoleApp", "namespace_console_app.html", [
      [ "Program", "class_console_app_1_1_program.html", null ]
    ] ],
    [ "Data", "namespace_data.html", [
      [ "GymContext", "class_data_1_1_gym_context.html", "class_data_1_1_gym_context" ]
    ] ],
    [ "Logic", "namespace_logic.html", [
      [ "Test", "namespace_logic_1_1_test.html", [
        [ "LogicTest", "class_logic_1_1_test_1_1_logic_test.html", "class_logic_1_1_test_1_1_logic_test" ]
      ] ],
      [ "ClientLogic", "class_logic_1_1_client_logic.html", "class_logic_1_1_client_logic" ],
      [ "ExtraInfoLogic", "class_logic_1_1_extra_info_logic.html", "class_logic_1_1_extra_info_logic" ],
      [ "TrainerLogic", "class_logic_1_1_trainer_logic.html", "class_logic_1_1_trainer_logic" ]
    ] ],
    [ "Models", "namespace_models.html", [
      [ "ExtraInfo", "class_models_1_1_extra_info.html", "class_models_1_1_extra_info" ],
      [ "GymClient", "class_models_1_1_gym_client.html", "class_models_1_1_gym_client" ],
      [ "Statistics", "class_models_1_1_statistics.html", "class_models_1_1_statistics" ],
      [ "Trainer", "class_models_1_1_trainer.html", "class_models_1_1_trainer" ]
    ] ],
    [ "Repos", "namespace_repos.html", [
      [ "ClientRepository", "class_repos_1_1_client_repository.html", "class_repos_1_1_client_repository" ],
      [ "InfoRepository", "class_repos_1_1_info_repository.html", "class_repos_1_1_info_repository" ],
      [ "IRepoBase", "interface_repos_1_1_i_repo_base.html", "interface_repos_1_1_i_repo_base" ],
      [ "TrainerRepository", "class_repos_1_1_trainer_repository.html", "class_repos_1_1_trainer_repository" ]
    ] ],
    [ "WebApplication", "namespace_web_application.html", [
      [ "Controllers", "namespace_web_application_1_1_controllers.html", [
        [ "HomeController", "class_web_application_1_1_controllers_1_1_home_controller.html", "class_web_application_1_1_controllers_1_1_home_controller" ]
      ] ],
      [ "Program", "class_web_application_1_1_program.html", "class_web_application_1_1_program" ],
      [ "Startup", "class_web_application_1_1_startup.html", "class_web_application_1_1_startup" ]
    ] ],
    [ "WpfApp", "namespace_wpf_app.html", [
      [ "BL", "namespace_wpf_app_1_1_b_l.html", [
        [ "IEditorService", "interface_wpf_app_1_1_b_l_1_1_i_editor_service.html", "interface_wpf_app_1_1_b_l_1_1_i_editor_service" ],
        [ "IUITrainerLogic", "interface_wpf_app_1_1_b_l_1_1_i_u_i_trainer_logic.html", "interface_wpf_app_1_1_b_l_1_1_i_u_i_trainer_logic" ],
        [ "UITrainerLogic", "class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic.html", "class_wpf_app_1_1_b_l_1_1_u_i_trainer_logic" ]
      ] ],
      [ "DataGeneration", "namespace_wpf_app_1_1_data_generation.html", [
        [ "TestDataGeneration", "class_wpf_app_1_1_data_generation_1_1_test_data_generation.html", "class_wpf_app_1_1_data_generation_1_1_test_data_generation" ]
      ] ],
      [ "UI", "namespace_wpf_app_1_1_u_i.html", [
        [ "EditorWindow", "class_wpf_app_1_1_u_i_1_1_editor_window.html", "class_wpf_app_1_1_u_i_1_1_editor_window" ],
        [ "EditorServiceViaWindow", "class_wpf_app_1_1_u_i_1_1_editor_service_via_window.html", "class_wpf_app_1_1_u_i_1_1_editor_service_via_window" ]
      ] ],
      [ "UserInterface", "namespace_wpf_app_1_1_user_interface.html", [
        [ "EditorWindow", "class_wpf_app_1_1_user_interface_1_1_editor_window.html", "class_wpf_app_1_1_user_interface_1_1_editor_window" ],
        [ "StatusToBrushConverter", "class_wpf_app_1_1_user_interface_1_1_status_to_brush_converter.html", "class_wpf_app_1_1_user_interface_1_1_status_to_brush_converter" ]
      ] ],
      [ "VM", "namespace_wpf_app_1_1_v_m.html", [
        [ "EditorViewModel", "class_wpf_app_1_1_v_m_1_1_editor_view_model.html", "class_wpf_app_1_1_v_m_1_1_editor_view_model" ],
        [ "MainViewModel", "class_wpf_app_1_1_v_m_1_1_main_view_model.html", "class_wpf_app_1_1_v_m_1_1_main_view_model" ]
      ] ],
      [ "App", "class_wpf_app_1_1_app.html", "class_wpf_app_1_1_app" ],
      [ "MainWindow", "class_wpf_app_1_1_main_window.html", "class_wpf_app_1_1_main_window" ],
      [ "MyIoc", "class_wpf_app_1_1_my_ioc.html", "class_wpf_app_1_1_my_ioc" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];