var class_logic_1_1_trainer_logic =
[
    [ "TrainerLogic", "class_logic_1_1_trainer_logic.html#ab2be722db1a57a8eacf7962e09f8647f", null ],
    [ "AddClientToTrainer", "class_logic_1_1_trainer_logic.html#ae776b08a56be5011f9f724efdb0ddb8c", null ],
    [ "AddTrainer", "class_logic_1_1_trainer_logic.html#a24bd0851dae26410d4f2241ff8fb719d", null ],
    [ "AmountOfClients", "class_logic_1_1_trainer_logic.html#aebb893aa8c23759035e08d1b5fa59a5c", null ],
    [ "AmountOfExtraInfo", "class_logic_1_1_trainer_logic.html#a7bce1dc1b0cc7d65d7c4bb27180fd15e", null ],
    [ "AmountOfTrainers", "class_logic_1_1_trainer_logic.html#a954524086d5473e7a9af19780a74e83c", null ],
    [ "AverageAge", "class_logic_1_1_trainer_logic.html#a777bcd0618bbed9e220105d1c9a3c040", null ],
    [ "DeleteTrainer", "class_logic_1_1_trainer_logic.html#a85a3b774077798c8ec28ae125cfe801e", null ],
    [ "FillDbWithSamples", "class_logic_1_1_trainer_logic.html#a14be7d16531f4a5072fb7698355eaad7", null ],
    [ "GenderPercentage", "class_logic_1_1_trainer_logic.html#ada42a0a337bfa5718f32269a58539db8", null ],
    [ "GetTrainer", "class_logic_1_1_trainer_logic.html#ab52a989b064a9a48e56c9a96ecc1b5ca", null ],
    [ "GetTrainers", "class_logic_1_1_trainer_logic.html#a37feeedb11457f34092ade7fe4a6250b", null ],
    [ "RemoveClientFromTrainer", "class_logic_1_1_trainer_logic.html#a088c1fe51c976504cc0e36a7ad6d51df", null ],
    [ "UpdateTrainer", "class_logic_1_1_trainer_logic.html#a547f349145c7bb8138b2798792aff098", null ]
];