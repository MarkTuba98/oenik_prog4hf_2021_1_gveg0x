var namespaces_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", "namespace_asp_net_core" ],
    [ "ConsoleApp", "namespace_console_app.html", "namespace_console_app" ],
    [ "Data", "namespace_data.html", "namespace_data" ],
    [ "Logic", "namespace_logic.html", "namespace_logic" ],
    [ "Models", "namespace_models.html", "namespace_models" ],
    [ "Repos", "namespace_repos.html", "namespace_repos" ],
    [ "WebApplication", "namespace_web_application.html", "namespace_web_application" ],
    [ "WpfApp", "namespace_wpf_app.html", "namespace_wpf_app" ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", "namespace_xaml_generated_namespace" ]
];